<?php
// created: 2015-04-23 11:00:13
$mod_strings = array (
  'LBL_DESCRIPTION' => 'Descripción lineas de Producto',
  'LBL_QUICKCREATE_PANEL1' => 'default',
  'LBL_NAME' => 'Name',
  'LNK_NEW_RECORD' => 'Create Product lines (C.Comerciales)',
  'LNK_LIST' => 'View Product lines (C.Comerciales)',
  'LNK_IMPORT_A123_LINEASDEPRODUCTOS' => 'Importar Product lines (C.Comerciales)',
  'LBL_LIST_FORM_TITLE' => 'Product lines (C.Comerciales) List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Product lines (C.Comerciales)',
);