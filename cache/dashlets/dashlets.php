<?php
// created: 2015-05-03 17:13:59
$dashletsFiles = array (
  'MyCallsDashlet' => 
  array (
    'file' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.php',
    'class' => 'MyCallsDashlet',
    'meta' => 'modules/Calls/Dashlets/MyCallsDashlet/MyCallsDashlet.meta.php',
    'module' => 'Calls',
  ),
  'SugarFeedDashlet' => 
  array (
    'file' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.php',
    'class' => 'SugarFeedDashlet',
    'meta' => 'modules/SugarFeed/Dashlets/SugarFeedDashlet/SugarFeedDashlet.meta.php',
    'module' => 'SugarFeed',
  ),
  'MyContactsDashlet' => 
  array (
    'file' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.php',
    'class' => 'MyContactsDashlet',
    'meta' => 'modules/Contacts/Dashlets/MyContactsDashlet/MyContactsDashlet.meta.php',
    'module' => 'Contacts',
  ),
  'a123_LineasdeproductosDashlet' => 
  array (
    'file' => 'modules/a123_Lineasdeproductos/Dashlets/a123_LineasdeproductosDashlet/a123_LineasdeproductosDashlet.php',
    'class' => 'a123_LineasdeproductosDashlet',
    'meta' => 'modules/a123_Lineasdeproductos/Dashlets/a123_LineasdeproductosDashlet/a123_LineasdeproductosDashlet.meta.php',
    'module' => 'a123_Lineasdeproductos',
  ),
  'MyProjectTaskDashlet' => 
  array (
    'file' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.php',
    'class' => 'MyProjectTaskDashlet',
    'meta' => 'modules/ProjectTask/Dashlets/MyProjectTaskDashlet/MyProjectTaskDashlet.meta.php',
    'module' => 'ProjectTask',
  ),
  'MyMeetingsDashlet' => 
  array (
    'file' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.php',
    'class' => 'MyMeetingsDashlet',
    'meta' => 'modules/Meetings/Dashlets/MyMeetingsDashlet/MyMeetingsDashlet.meta.php',
    'module' => 'Meetings',
  ),
  'TopCampaignsDashlet' => 
  array (
    'file' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.php',
    'class' => 'TopCampaignsDashlet',
    'meta' => 'modules/Campaigns/Dashlets/TopCampaignsDashlet/TopCampaignsDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'a123_ContactoEnLaEmpresaDashlet' => 
  array (
    'file' => 'modules/a123_ContactoEnLaEmpresa/Dashlets/a123_ContactoEnLaEmpresaDashlet/a123_ContactoEnLaEmpresaDashlet.php',
    'class' => 'a123_ContactoEnLaEmpresaDashlet',
    'meta' => 'modules/a123_ContactoEnLaEmpresa/Dashlets/a123_ContactoEnLaEmpresaDashlet/a123_ContactoEnLaEmpresaDashlet.meta.php',
    'module' => 'a123_ContactoEnLaEmpresa',
  ),
  'MyBugsDashlet' => 
  array (
    'file' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.php',
    'class' => 'MyBugsDashlet',
    'meta' => 'modules/Bugs/Dashlets/MyBugsDashlet/MyBugsDashlet.meta.php',
    'module' => 'Bugs',
  ),
  'MyNotesDashlet' => 
  array (
    'file' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.php',
    'class' => 'MyNotesDashlet',
    'meta' => 'modules/Notes/Dashlets/MyNotesDashlet/MyNotesDashlet.meta.php',
    'module' => 'Notes',
  ),
  'Cal_calculoDashlet' => 
  array (
    'file' => 'modules/Cal_calculo/Dashlets/Cal_calculoDashlet/Cal_calculoDashlet.php',
    'class' => 'Cal_calculoDashlet',
    'meta' => 'modules/Cal_calculo/Dashlets/Cal_calculoDashlet/Cal_calculoDashlet.meta.php',
    'module' => 'Cal_calculo',
  ),
  'MyTasksDashlet' => 
  array (
    'file' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.php',
    'class' => 'MyTasksDashlet',
    'meta' => 'modules/Tasks/Dashlets/MyTasksDashlet/MyTasksDashlet.meta.php',
    'module' => 'Tasks',
  ),
  'fin_calculofinancieroDashlet' => 
  array (
    'file' => 'modules/fin_calculofinanciero/Dashlets/fin_calculofinancieroDashlet/fin_calculofinancieroDashlet.php',
    'class' => 'fin_calculofinancieroDashlet',
    'meta' => 'modules/fin_calculofinanciero/Dashlets/fin_calculofinancieroDashlet/fin_calculofinancieroDashlet.meta.php',
    'module' => 'fin_calculofinanciero',
  ),
  'reg_invoicesDashlet' => 
  array (
    'file' => 'modules/reg_invoices/modules/reg_invoices/Dashlets/reg_invoicesDashlet/reg_invoicesDashlet.php',
    'class' => 'reg_invoicesDashlet',
    'meta' => 'modules/reg_invoices/modules/reg_invoices/Dashlets/reg_invoicesDashlet/reg_invoicesDashlet.meta.php',
    'module' => 'reg_invoices',
  ),
  'q1w2e_probando2Dashlet' => 
  array (
    'file' => 'modules/q1w2e_probando2/Dashlets/q1w2e_probando2Dashlet/q1w2e_probando2Dashlet.php',
    'class' => 'q1w2e_probando2Dashlet',
    'meta' => 'modules/q1w2e_probando2/Dashlets/q1w2e_probando2Dashlet/q1w2e_probando2Dashlet.meta.php',
    'module' => 'q1w2e_probando2',
  ),
  'MyAccountsDashlet' => 
  array (
    'file' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.php',
    'class' => 'MyAccountsDashlet',
    'meta' => 'modules/Accounts/Dashlets/MyAccountsDashlet/MyAccountsDashlet.meta.php',
    'module' => 'Accounts',
  ),
  'Rcc_ReferenciasC_empresas_compraDashlet' => 
  array (
    'file' => 'modules/Rcc_ReferenciasC_empresas_compra/Dashlets/Rcc_ReferenciasC_empresas_compraDashlet/Rcc_ReferenciasC_empresas_compraDashlet.php',
    'class' => 'Rcc_ReferenciasC_empresas_compraDashlet',
    'meta' => 'modules/Rcc_ReferenciasC_empresas_compra/Dashlets/Rcc_ReferenciasC_empresas_compraDashlet/Rcc_ReferenciasC_empresas_compraDashlet.meta.php',
    'module' => 'Rcc_ReferenciasC_empresas_compra',
  ),
  'CalendarDashlet' => 
  array (
    'file' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.php',
    'class' => 'CalendarDashlet',
    'meta' => 'modules/Calendar/Dashlets/CalendarDashlet/CalendarDashlet.meta.php',
  ),
  'idc_InformacionDeComercioDashlet' => 
  array (
    'file' => 'modules/idc_InformacionDeComercio/Dashlets/idc_InformacionDeComercioDashlet/idc_InformacionDeComercioDashlet.php',
    'class' => 'idc_InformacionDeComercioDashlet',
    'meta' => 'modules/idc_InformacionDeComercio/Dashlets/idc_InformacionDeComercioDashlet/idc_InformacionDeComercioDashlet.meta.php',
    'module' => 'idc_InformacionDeComercio',
  ),
  'MyEmailsDashlet' => 
  array (
    'file' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.php',
    'class' => 'MyEmailsDashlet',
    'meta' => 'modules/Emails/Dashlets/MyEmailsDashlet/MyEmailsDashlet.meta.php',
    'module' => 'Emails',
  ),
  'q1w2e_probandoDashlet' => 
  array (
    'file' => 'modules/q1w2e_probando/Dashlets/q1w2e_probandoDashlet/q1w2e_probandoDashlet.php',
    'class' => 'q1w2e_probandoDashlet',
    'meta' => 'modules/q1w2e_probando/Dashlets/q1w2e_probandoDashlet/q1w2e_probandoDashlet.meta.php',
    'module' => 'q1w2e_probando',
  ),
  'OpportunitiesByLeadSourceDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.php',
    'class' => 'OpportunitiesByLeadSourceDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceDashlet/OpportunitiesByLeadSourceDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OpportunitiesByLeadSourceByOutcomeDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.php',
    'class' => 'OpportunitiesByLeadSourceByOutcomeDashlet',
    'meta' => 'modules/Charts/Dashlets/OpportunitiesByLeadSourceByOutcomeDashlet/OpportunitiesByLeadSourceByOutcomeDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'CampaignROIChartDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.php',
    'class' => 'CampaignROIChartDashlet',
    'meta' => 'modules/Charts/Dashlets/CampaignROIChartDashlet/CampaignROIChartDashlet.meta.php',
    'module' => 'Campaigns',
  ),
  'MyPipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.php',
    'class' => 'MyPipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/MyPipelineBySalesStageDashlet/MyPipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'OutcomeByMonthDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.php',
    'class' => 'OutcomeByMonthDashlet',
    'meta' => 'modules/Charts/Dashlets/OutcomeByMonthDashlet/OutcomeByMonthDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'PipelineBySalesStageDashlet' => 
  array (
    'file' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.php',
    'class' => 'PipelineBySalesStageDashlet',
    'meta' => 'modules/Charts/Dashlets/PipelineBySalesStageDashlet/PipelineBySalesStageDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'a123_paisDashlet' => 
  array (
    'file' => 'modules/a123_pais/Dashlets/a123_paisDashlet/a123_paisDashlet.php',
    'class' => 'a123_paisDashlet',
    'meta' => 'modules/a123_pais/Dashlets/a123_paisDashlet/a123_paisDashlet.meta.php',
    'module' => 'a123_pais',
  ),
  'SugarNewsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.php',
    'class' => 'SugarNewsDashlet',
    'meta' => 'modules/Home/Dashlets/SugarNewsDashlet/SugarNewsDashlet.meta.php',
    'module' => 'Home',
  ),
  'iFrameDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.php',
    'class' => 'iFrameDashlet',
    'meta' => 'modules/Home/Dashlets/iFrameDashlet/iFrameDashlet.meta.php',
    'module' => 'Home',
  ),
  'ChartsDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.php',
    'class' => 'ChartsDashlet',
    'meta' => 'modules/Home/Dashlets/ChartsDashlet/ChartsDashlet.meta.php',
  ),
  'RSSDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.php',
    'class' => 'RSSDashlet',
    'meta' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/RSSDashlet/RSSDashlet.icon.jpg',
  ),
  'JotPadDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.php',
    'class' => 'JotPadDashlet',
    'meta' => 'modules/Home/Dashlets/JotPadDashlet/JotPadDashlet.meta.php',
  ),
  'InvadersDashlet' => 
  array (
    'file' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.php',
    'class' => 'InvadersDashlet',
    'meta' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.meta.php',
    'icon' => 'modules/Home/Dashlets/InvadersDashlet/InvadersDashlet.icon.jpg',
  ),
  'a123_ReferenciascomercialesDashlet' => 
  array (
    'file' => 'modules/a123_Referenciascomerciales/Dashlets/a123_ReferenciascomercialesDashlet/a123_ReferenciascomercialesDashlet.php',
    'class' => 'a123_ReferenciascomercialesDashlet',
    'meta' => 'modules/a123_Referenciascomerciales/Dashlets/a123_ReferenciascomercialesDashlet/a123_ReferenciascomercialesDashlet.meta.php',
    'module' => 'a123_Referenciascomerciales',
  ),
  'MyLeadsDashlet' => 
  array (
    'file' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.php',
    'class' => 'MyLeadsDashlet',
    'meta' => 'modules/Leads/Dashlets/MyLeadsDashlet/MyLeadsDashlet.meta.php',
    'module' => 'Leads',
  ),
  'MyDocumentsDashlet' => 
  array (
    'file' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.php',
    'class' => 'MyDocumentsDashlet',
    'meta' => 'modules/Documents/Dashlets/MyDocumentsDashlet/MyDocumentsDashlet.meta.php',
    'module' => 'Documents',
  ),
  'MyOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.php',
    'class' => 'MyOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyOpportunitiesDashlet/MyOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyClosedOpportunitiesDashlet' => 
  array (
    'file' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.php',
    'class' => 'MyClosedOpportunitiesDashlet',
    'meta' => 'modules/Opportunities/Dashlets/MyClosedOpportunitiesDashlet/MyClosedOpportunitiesDashlet.meta.php',
    'module' => 'Opportunities',
  ),
  'MyCasesDashlet' => 
  array (
    'file' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.php',
    'class' => 'MyCasesDashlet',
    'meta' => 'modules/Cases/Dashlets/MyCasesDashlet/MyCasesDashlet.meta.php',
    'module' => 'Cases',
  ),
);