<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-02-17 10:59:33
$dictionary['a123_Lineasdeproductos']['fields']['pais_c']['labelValue']='Pais';

 

 // created: 2015-02-23 11:31:12
$dictionary['a123_Lineasdeproductos']['fields']['name']['required']=false;
$dictionary['a123_Lineasdeproductos']['fields']['name']['duplicate_merge']='disabled';
$dictionary['a123_Lineasdeproductos']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['a123_Lineasdeproductos']['fields']['name']['merge_filter']='disabled';
$dictionary['a123_Lineasdeproductos']['fields']['name']['unified_search']=false;

 

// created: 2015-02-16 08:54:25
$dictionary["a123_Lineasdeproductos"]["fields"]["a123_lineasdeproductos_contacts"] = array (
  'name' => 'a123_lineasdeproductos_contacts',
  'type' => 'link',
  'relationship' => 'a123_lineasdeproductos_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_CONTACTS_TITLE',
);


 // created: 2015-02-24 10:40:13
$dictionary['a123_Lineasdeproductos']['fields']['description']['comments']='Full text of the note';
$dictionary['a123_Lineasdeproductos']['fields']['description']['merge_filter']='disabled';
$dictionary['a123_Lineasdeproductos']['fields']['description']['rows']='20';
$dictionary['a123_Lineasdeproductos']['fields']['description']['cols']='50';

 

 // created: 2015-03-04 08:59:14
$dictionary['a123_Lineasdeproductos']['fields']['sucursal_c']['labelValue']='Numero de sucursales';

 
?>