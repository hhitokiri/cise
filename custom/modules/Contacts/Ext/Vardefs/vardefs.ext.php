<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-02-23 13:42:31
$dictionary['Contact']['fields']['for_pago_c']['labelValue']='Forma de pago';

 

 // created: 2015-02-23 14:02:53
$dictionary['Contact']['fields']['fecha_req_c']['labelValue']='Fecha Requerida';

 

 // created: 2015-03-11 16:30:27
$dictionary['Contact']['fields']['sec_interes_c']['labelValue']='Sector De Interes Para la Inversion:';

 

 // created: 2015-02-18 10:22:51
$dictionary['Contact']['fields']['direccion_emp_c']['labelValue']='Direccion De Empresa';

 

 // created: 2015-03-03 14:30:30
$dictionary['Contact']['fields']['serv_ofre_c']['labelValue']='Servicios ofrecidos:';

 

 // created: 2015-02-23 13:45:35
$dictionary['Contact']['fields']['confeccion_c']['labelValue']='Sector Confeccion';

 

// created: 2015-02-16 08:54:25
$dictionary["Contact"]["fields"]["a123_lineasdeproductos_contacts"] = array (
  'name' => 'a123_lineasdeproductos_contacts',
  'type' => 'link',
  'relationship' => 'a123_lineasdeproductos_contacts',
  'source' => 'non-db',
  'module' => 'a123_Lineasdeproductos',
  'bean_name' => 'a123_Lineasdeproductos',
  'vname' => 'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_A123_LINEASDEPRODUCTOS_TITLE',
);


 // created: 2015-02-11 11:39:33
$dictionary['Contact']['fields']['facebook_emp_c']['labelValue']='Facebook';

 

 // created: 2015-02-23 13:34:46
$dictionary['Contact']['fields']['espec_tecnicas_c']['labelValue']='Especificaciones Tecnicas';

 

 // created: 2015-02-18 10:20:47
$dictionary['Contact']['fields']['nombre_emp_c']['labelValue']='Nombre De Empresa';

 

 // created: 2015-04-24 09:04:10
$dictionary['Contact']['fields']['otra_actividad_c']['labelValue']='Otra Actividad';

 

// created: 2015-02-27 08:54:27
$dictionary["Contact"]["fields"]["contacts_rcc_referenciasc_empresas_compra_1"] = array (
  'name' => 'contacts_rcc_referenciasc_empresas_compra_1',
  'type' => 'link',
  'relationship' => 'contacts_rcc_referenciasc_empresas_compra_1',
  'source' => 'non-db',
  'module' => 'Rcc_ReferenciasC_empresas_compra',
  'bean_name' => 'Rcc_ReferenciasC_empresas_compra',
  'vname' => 'LBL_CONTACTS_RCC_REFERENCIASC_EMPRESAS_COMPRA_1_FROM_RCC_REFERENCIASC_EMPRESAS_COMPRA_TITLE',
);


 // created: 2015-03-04 09:18:09
$dictionary['Contact']['fields']['otro_contac_c']['labelValue']='Especifique otra forma de generación de contacto:';

 

 // created: 2015-03-11 11:17:08
$dictionary['Contact']['fields']['act_emp_c']['labelValue']='Actividad';

 

 // created: 2015-02-23 13:35:50
$dictionary['Contact']['fields']['uni_med_c']['labelValue']='Unidad de Medida';

 

 // created: 2015-02-23 13:38:34
$dictionary['Contact']['fields']['frac_aran_c']['labelValue']='Fracción Arancelaria';

 

 // created: 2015-03-03 14:18:41
$dictionary['Contact']['fields']['correo2_c']['labelValue']='Email';

 

 // created: 2015-03-03 14:15:59
$dictionary['Contact']['fields']['idioma2_c']['labelValue']='Idioma';

 

 // created: 2015-03-03 14:33:54
$dictionary['Contact']['fields']['especifique_medio_c']['labelValue']='Especifique tipo de medio:';

 

 // created: 2015-02-23 13:17:14
$dictionary['Contact']['fields']['agremiados_c']['labelValue']='Agremiados o Socios';

 

 // created: 2015-03-11 15:42:07

 

 // created: 2015-03-03 14:23:24
$dictionary['Contact']['fields']['pertenece_c']['labelValue']='Pertenece a una camara';

 

 // created: 2015-02-23 13:16:10
$dictionary['Contact']['fields']['objetivo_c']['labelValue']='Objetivo';

 

 // created: 2015-02-11 12:57:35
$dictionary['Contact']['fields']['miembro_comer_c']['labelValue']='Miembros de camara de comercio y/o Asociasiones';

 

 // created: 2015-02-23 13:34:10
$dictionary['Contact']['fields']['caracter_c']['labelValue']='Caracteristicas';

 

 // created: 2015-02-23 13:29:37
$dictionary['Contact']['fields']['tip_medio_c']['labelValue']='Tipo de Medio';

 

// created: 2015-03-03 13:38:58
$dictionary["Contact"]["fields"]["contacts_a123_referenciascomerciales_1"] = array (
  'name' => 'contacts_a123_referenciascomerciales_1',
  'type' => 'link',
  'relationship' => 'contacts_a123_referenciascomerciales_1',
  'source' => 'non-db',
  'module' => 'a123_Referenciascomerciales',
  'bean_name' => 'a123_Referenciascomerciales',
  'vname' => 'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_A123_REFERENCIASCOMERCIALES_TITLE',
);


 // created: 2015-03-11 15:49:07
$dictionary['Contact']['fields']['nombre_cmatriz_c']['labelValue']='Nombre Casa Matriz';

 

 // created: 2015-03-11 16:32:31
$dictionary['Contact']['fields']['otro_sectorinte_c']['labelValue']='Otro sector de interés ';

 

 // created: 2015-03-10 12:54:13
$dictionary['Contact']['fields']['pais_proc_c']['labelValue']='País de procedencia';

 

 // created: 2015-03-03 14:27:51
$dictionary['Contact']['fields']['monto_aproinv_c']['labelValue']='Monto Aproximado $USD:';

 

 // created: 2015-03-11 10:20:25
$dictionary['Contact']['fields']['cod_area1_c']['labelValue']='Código área';

 

 // created: 2015-02-11 12:58:57
$dictionary['Contact']['fields']['observ_c']['labelValue']='Observaciones';

 

 // created: 2015-03-11 15:42:08

 

 // created: 2015-03-11 16:43:09
$dictionary['Contact']['fields']['op_inversion_c']['labelValue']='Oportunidad de Inversion';

 

 // created: 2015-03-03 14:20:41
$dictionary['Contact']['fields']['telefono2_c']['labelValue']='Telefono';

 

 // created: 2015-04-22 08:58:59
$dictionary['Contact']['fields']['otra_opor_c']['labelValue']='Otra Oportunidad (Especifique)';

 

 // created: 2015-02-23 11:24:35
$dictionary['Contact']['fields']['description']['required']=true;
$dictionary['Contact']['fields']['description']['comments']='Full text of the note';
$dictionary['Contact']['fields']['description']['merge_filter']='disabled';

 

 // created: 2015-02-18 10:17:45
$dictionary['Contact']['fields']['email1']['required']=true;
$dictionary['Contact']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2015-03-11 15:42:08
$dictionary['Contact']['fields']['direccion_cmatri_c']['labelValue']='Dirección casa matriz';

 

 // created: 2015-02-18 10:23:36
$dictionary['Contact']['fields']['web_emp_c']['labelValue']='Web Site';

 

 // created: 2015-02-18 10:21:18
$dictionary['Contact']['fields']['telefono_emp_c']['labelValue']='Telefono De Empresa';
//$dictionary['Contact']['fields']['telefono_emp_c']['type']='int';

 

 // created: 2015-03-17 11:52:44
$dictionary['Contact']['fields']['alt_address_postalcode']['comments']='Postal code for alternate address';
$dictionary['Contact']['fields']['alt_address_postalcode']['merge_filter']='disabled';

 

 // created: 2015-03-11 15:42:07

 

 // created: 2015-03-04 09:02:16
$dictionary['Contact']['fields']['costos_c']['labelValue']='Costos';

 

 // created: 2015-02-18 10:15:59
$dictionary['Contact']['fields']['first_name']['required']=true;
$dictionary['Contact']['fields']['first_name']['comments']='First name of the contact';
$dictionary['Contact']['fields']['first_name']['merge_filter']='disabled';

 

 // created: 2015-02-18 10:16:59
$dictionary['Contact']['fields']['title']['required']=true;
$dictionary['Contact']['fields']['title']['comments']='The title of the contact';
$dictionary['Contact']['fields']['title']['merge_filter']='disabled';

 

 // created: 2015-03-12 09:36:26
$dictionary['Contact']['fields']['razon_soc_c']['labelValue']='Tipo de producto:';

 

 // created: 2015-03-04 09:16:15
$dictionary['Contact']['fields']['cont_generado_c']['labelValue']='Contacto generado en';

 

 // created: 2015-02-11 12:23:07

 

 // created: 2015-03-03 14:25:09
$dictionary['Contact']['fields']['salutation']['len']=100;
$dictionary['Contact']['fields']['salutation']['comments']='Contact salutation (e.g., Mr, Ms)';
$dictionary['Contact']['fields']['salutation']['merge_filter']='disabled';

 

 // created: 2015-03-11 16:22:22
$dictionary['Contact']['fields']['movil_c']['labelValue']='Movil';

 

 // created: 2015-02-11 11:40:37
$dictionary['Contact']['fields']['twitter_emp_c']['labelValue']='Twitter';

 

 // created: 2015-03-03 14:39:00
$dictionary['Contact']['fields']['obser_suge_c']['labelValue']='Observaciones o Sugerencias';

 

 // created: 2015-02-11 12:33:28
$dictionary['Contact']['fields']['nu_emp_c']['labelValue']='Numero De Empleados';
//$dictionary['Contact']['fields']['nu_emp_c']['type']='int';
 

 // created: 2015-02-23 13:36:35
$dictionary['Contact']['fields']['vol_des_c']['labelValue']='Volumen deseado';

 

 // created: 2015-02-23 13:14:39
$dictionary['Contact']['fields']['sector_c']['labelValue']='Sector';

 

 // created: 2015-03-03 14:17:13
$dictionary['Contact']['fields']['cargo2_c']['labelValue']='Cargo';

 

 // created: 2015-02-11 12:27:21
$dictionary['Contact']['fields']['fecha_proinv_c']['labelValue']='Año Proyectado Para La Inversion';

 

 // created: 2015-02-11 12:32:39
$dictionary['Contact']['fields']['tiempo_inv_c']['labelValue']='Tiempo Previsto Para La Inversion';

 

 // created: 2015-03-03 14:35:21
$dictionary['Contact']['fields']['obs_sugerencias_c']['labelValue']='obs sugerencias';

 

 // created: 2015-03-03 14:29:16
$dictionary['Contact']['fields']['produc_manuf_c']['labelValue']='Productos que manufactura:';

 

 // created: 2015-03-03 14:19:43
$dictionary['Contact']['fields']['nombre2_c']['labelValue']='Nombre';

 

 // created: 2015-03-11 09:45:30
$dictionary['Contact']['fields']['cod_area_c']['labelValue']='Código de área / ZIP';
$dictionary['Contact']['fields']['cod_area_c']['type']='int';
 

 // created: 2015-02-16 08:20:28
$dictionary['Contact']['fields']['tipo_contac_c']['labelValue']='Tipo de Contacto';

 

 // created: 2015-02-18 10:23:14
$dictionary['Contact']['fields']['fax_emp_c']['labelValue']='Fax De La Empresa';

 

 // created: 2015-03-03 14:15:36
$dictionary['Contact']['fields']['apellido2_c']['labelValue']='Apellido';

 

 // created: 2015-02-11 13:05:31
$dictionary['Contact']['fields']['sugere_c']['labelValue']='Sugerencias';

 

 // created: 2015-02-18 10:16:37
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['audited']=false;

 

 // created: 2015-03-11 15:42:08

 

 // created: 2015-02-23 13:18:42
$dictionary['Contact']['fields']['obser_sugerencias_c']['labelValue']='Observaciones o Sugerencias';

 

 // created: 2015-02-18 13:58:25
$dictionary['Contact']['fields']['info_emp_c']['labelValue']='Información de la Empresa';

 

 // created: 2015-02-23 13:43:09
$dictionary['Contact']['fields']['req_esp_c']['labelValue']='Requerimientos Especiales';

 

// created: 2015-02-16 08:58:28
$dictionary["Contact"]["fields"]["a123_contactoenlaempresa_contacts_1"] = array (
  'name' => 'a123_contactoenlaempresa_contacts_1',
  'type' => 'link',
  'relationship' => 'a123_contactoenlaempresa_contacts_1',
  'source' => 'non-db',
  'module' => 'a123_ContactoEnLaEmpresa',
  'bean_name' => 'a123_ContactoEnLaEmpresa',
  'vname' => 'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_A123_CONTACTOENLAEMPRESA_TITLE',
);

?>