<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-03-11 12:12:28
$dictionary['a123_Referenciascomerciales']['fields']['nombre_empresa_c']['labelValue']='Nombre de la empresa';

 

 // created: 2015-02-18 15:10:43
$dictionary['a123_Referenciascomerciales']['fields']['alc_geo1_c']['labelValue']='Alcance geográfico ';

 

 // created: 2015-02-18 15:06:33
$dictionary['a123_Referenciascomerciales']['fields']['alc_geo']['required']=false;

 

 // created: 2015-02-18 14:43:22
$dictionary['a123_Referenciascomerciales']['fields']['nombre_contactv']['required']=true;

 

 // created: 2015-02-18 14:45:15
$dictionary['a123_Referenciascomerciales']['fields']['paisc_c']['labelValue']='Pais';

 

 // created: 2015-03-11 12:13:18
$dictionary['a123_Referenciascomerciales']['fields']['ven_anu']['required']=false;

 

 // created: 2015-02-23 11:46:30
$dictionary['a123_Referenciascomerciales']['fields']['tip_pro']['required']=false;

 

 // created: 2015-02-18 14:43:54
$dictionary['a123_Referenciascomerciales']['fields']['nu_bode']['required']=true;

 

 // created: 2015-02-18 14:43:27
$dictionary['a123_Referenciascomerciales']['fields']['cargo_v']['required']=true;

 

 // created: 2015-02-18 14:43:30
$dictionary['a123_Referenciascomerciales']['fields']['tel_v']['required']=true;

 

// created: 2015-03-03 13:38:58
$dictionary["a123_Referenciascomerciales"]["fields"]["contacts_a123_referenciascomerciales_1"] = array (
  'name' => 'contacts_a123_referenciascomerciales_1',
  'type' => 'link',
  'relationship' => 'contacts_a123_referenciascomerciales_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_CONTACTS_TITLE',
);


 // created: 2015-03-11 12:12:53
$dictionary['a123_Referenciascomerciales']['fields']['tipo_de_producto_c']['labelValue']='Tipo de producto que distribuye ';

 

 // created: 2015-02-18 14:43:33
$dictionary['a123_Referenciascomerciales']['fields']['email_v']['required']=true;

 
?>