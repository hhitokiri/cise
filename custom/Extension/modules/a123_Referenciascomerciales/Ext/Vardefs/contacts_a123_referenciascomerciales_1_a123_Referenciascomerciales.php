<?php
// created: 2015-03-03 13:38:58
$dictionary["a123_Referenciascomerciales"]["fields"]["contacts_a123_referenciascomerciales_1"] = array (
  'name' => 'contacts_a123_referenciascomerciales_1',
  'type' => 'link',
  'relationship' => 'contacts_a123_referenciascomerciales_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_CONTACTS_TITLE',
);
