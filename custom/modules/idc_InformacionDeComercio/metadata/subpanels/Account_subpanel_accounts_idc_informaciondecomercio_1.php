<?php
// created: 2015-03-23 12:09:29
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'monto_ventasnac_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_MONTO_VENTASNAC',
    'width' => '10%',
  ),
  'monto_expor_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_MONTO_EXPOR',
    'width' => '10%',
  ),
  'monto_import_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_MONTO_IMPORT',
    'width' => '10%',
  ),
  'destino_expor_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_DESTINO_EXPOR',
    'width' => '10%',
  ),
  'origen_import_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_ORIGEN_IMPORT',
    'width' => '10%',
  ),
  'exp_eventos_c' => 
  array (
    'type' => 'bool',
    'default' => true,
    'vname' => 'LBL_EXP_EVENTOS',
    'width' => '10%',
  ),
  'conq_entidad_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CONQ_ENTIDAD',
    'width' => '10%',
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'idc_InformacionDeComercio',
    'width' => '5%',
    'default' => true,
  ),
);