<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


class AccountsViewEdit extends ViewEdit
{
 	public function __construct()
 	{
 		parent::ViewEdit();
 		$this->useForSubpanel = true;
 		$this->useModuleQuickCreateTemplate = true;
                
 	}
        public function display(){
        parent::Display();
        echo <<<EOHTML
        
 <script src="include/javascript/chosen/chosen.jquery.js"></script>
 <link rel="stylesheet" href="include/javascript/chosen/chosen.css">
 <script src="include/javascript/cuentas.js"></script>       

EOHTML;
//$fraccion = $app_list_strings['fraccion_arancelaria_list'];
        $fraccion = $GLOBALS['app_list_strings']['fraccion_arancelaria_list']; 
       echo '<select name="name" id="cambio">';
       foreach ($fraccion as $key => $value) {
           echo '<option value="'.$key.'">'.$value.'</option>';
       }
       echo '</select>';
        echo"
       <script>        
       $('#name').replaceWith($('#cambio'));
       $('#cambio').attr('id','name');
       $('#name,#serv_comunicaciones_c,#serv_prestados_c,#idioma_pervia_c,#idioma1_pervia_c,#sector_economico_c,#acuerdos_c,#acuerdos_serv_c').chosen();

       </script>";
//$idiomas = $GLOBALS['app_list_strings']['idioma_list1'];        
    }
    
    

        }
        