<?php
$module_name = 'Cal_calculo';
$OBJECT_NAME = 'CAL_CALCULO';
$listViewDefs [$module_name] = 
array (
  'PAISES' => 
  array (
    'type' => 'multienum',
    'studio' => 'visible',
    'label' => 'LBL_PAISES',
    'width' => '10%',
    'default' => true,
    'link' => true,
  ),
  'RESPUESTA_A_REQUERIMIENTO' => 
  array (
    'type' => 'int',
    'label' => 'LBL_RESPUESTA_A_REQUERIMIENTO',
    'width' => '10%',
    'default' => true,
  ),
  'RESPUESTA_A_REQUERIMIENTO_N' => 
  array (
    'type' => 'int',
    'label' => 'LBL_RESPUESTA_A_REQUERIMIENTO_N',
    'width' => '10%',
    'default' => true,
  ),
  'OPORTUNIDADES_COMERCIALES' => 
  array (
    'type' => 'int',
    'label' => 'LBL_OPORTUNIDADES_COMERCIALES',
    'width' => '10%',
    'default' => true,
  ),
  'OPORTUNIDADES_COMERCIALES_N' => 
  array (
    'type' => 'int',
    'label' => 'LBL_OPORTUNIDADES_COMERCIALES_N',
    'width' => '10%',
    'default' => true,
  ),
  'POTENCIALES_INVERSIONISTAS' => 
  array (
    'type' => 'int',
    'label' => 'LBL_POTENCIALES_INVERSIONISTAS',
    'width' => '10%',
    'default' => true,
  ),
  'POTENCIALES_INVERSIONISTAS_N' => 
  array (
    'type' => 'int',
    'label' => 'LBL_POTENCIALES_INVERSIONISTAS_N',
    'width' => '10%',
    'default' => true,
  ),
  'ACTIVE_DATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_ACTIVE_DATE',
    'default' => true,
  ),
  'MODIFIED_BY_NAME' => 
  array (
    'width' => '10%',
    'label' => 'LBL_MODIFIED_USER',
    'module' => 'Users',
    'id' => 'USERS_ID',
    'default' => false,
    'sortable' => false,
    'related_fields' => 
    array (
      0 => 'modified_user_id',
    ),
  ),
  'DOCUMENT_NAME' => 
  array (
    'width' => '40%',
    'label' => 'LBL_NAME',
    'link' => true,
    'default' => false,
  ),
  'CREATED_BY_NAME' => 
  array (
    'width' => '2%',
    'label' => 'LBL_LIST_LAST_REV_CREATOR',
    'default' => false,
    'sortable' => false,
  ),
  'EXP_DATE' => 
  array (
    'width' => '10%',
    'label' => 'LBL_LIST_EXP_DATE',
    'default' => false,
  ),
  'SUBCATEGORY_ID' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_SUBCATEGORY',
    'default' => false,
  ),
  'CATEGORY_ID' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_CATEGORY',
    'default' => false,
  ),
);
?>
