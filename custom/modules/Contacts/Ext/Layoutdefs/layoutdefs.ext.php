<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-02-16 08:54:25
$layout_defs["Contacts"]["subpanel_setup"]['a123_lineasdeproductos_contacts'] = array (
  'order' => 100,
  'module' => 'a123_Lineasdeproductos',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_A123_LINEASDEPRODUCTOS_TITLE',
  'get_subpanel_data' => 'a123_lineasdeproductos_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2015-02-27 08:54:27
$layout_defs["Contacts"]["subpanel_setup"]['contacts_rcc_referenciasc_empresas_compra_1'] = array (
  'order' => 100,
  'module' => 'Rcc_ReferenciasC_empresas_compra',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_RCC_REFERENCIASC_EMPRESAS_COMPRA_1_FROM_RCC_REFERENCIASC_EMPRESAS_COMPRA_TITLE',
  'get_subpanel_data' => 'contacts_rcc_referenciasc_empresas_compra_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2015-03-03 13:38:58
$layout_defs["Contacts"]["subpanel_setup"]['contacts_a123_referenciascomerciales_1'] = array (
  'order' => 100,
  'module' => 'a123_Referenciascomerciales',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_A123_REFERENCIASCOMERCIALES_TITLE',
  'get_subpanel_data' => 'contacts_a123_referenciascomerciales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2015-02-16 08:58:28
$layout_defs["Contacts"]["subpanel_setup"]['a123_contactoenlaempresa_contacts_1'] = array (
  'order' => 100,
  'module' => 'a123_ContactoEnLaEmpresa',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_A123_CONTACTOENLAEMPRESA_TITLE',
  'get_subpanel_data' => 'a123_contactoenlaempresa_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['a123_lineasdeproductos_contacts_1']['override_subpanel_name'] = 'Contact_subpanel_a123_lineasdeproductos_contacts_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['a123_lineasdeproductos_contacts']['override_subpanel_name'] = 'Contact_subpanel_a123_lineasdeproductos_contacts';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['a123_contactoenlaempresa_contacts_1']['override_subpanel_name'] = 'Contact_subpanel_a123_contactoenlaempresa_contacts_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['a123_referenciascomerciales_contacts_1']['override_subpanel_name'] = 'Contact_subpanel_a123_referenciascomerciales_contacts_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_a123_referenciascomerciales_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_a123_referenciascomerciales_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['contacts_rcc_referenciasc_empresas_compra_1']['override_subpanel_name'] = 'Contact_subpanel_contacts_rcc_referenciasc_empresas_compra_1';


//auto-generated file DO NOT EDIT
$layout_defs['Contacts']['subpanel_setup']['opportunities']['override_subpanel_name'] = 'Contact_subpanel_opportunities';

?>