<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-03-12 12:19:57
$dictionary['idc_InformacionDeComercio']['fields']['monto_ventasnac_c']['labelValue']='Monto de ventas nacionales (USD)';

 

 // created: 2015-03-12 12:21:14
$dictionary['idc_InformacionDeComercio']['fields']['monto_import_c']['labelValue']='Monto de importaciones (USD)';

 

 // created: 2015-03-12 12:20:37
$dictionary['idc_InformacionDeComercio']['fields']['monto_expor_c']['labelValue']='Monto de exportaciones (USD)';

 

 // created: 2015-03-12 12:23:54
$dictionary['idc_InformacionDeComercio']['fields']['conq_entidad_c']['labelValue']='Con que entidad?';

 

 // created: 2015-03-12 12:22:25
$dictionary['idc_InformacionDeComercio']['fields']['origen_import_c']['labelValue']='Origen de las importaciones';

 

// created: 2015-03-12 12:35:05
$dictionary["idc_InformacionDeComercio"]["fields"]["accounts_idc_informaciondecomercio_1"] = array (
  'name' => 'accounts_idc_informaciondecomercio_1',
  'type' => 'link',
  'relationship' => 'accounts_idc_informaciondecomercio_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_IDC_INFORMACIONDECOMERCIO_1_FROM_ACCOUNTS_TITLE',
);


 // created: 2015-03-12 12:23:18
$dictionary['idc_InformacionDeComercio']['fields']['exp_eventos_c']['labelValue']='Ha tenido experiencia en eventos comerciales internacionales (Ferias, Ruedas, Misiones)?';

 

 // created: 2015-03-12 12:21:41
$dictionary['idc_InformacionDeComercio']['fields']['destino_expor_c']['labelValue']='Destino de las exportaciones';

 

 // created: 2015-03-12 15:00:37
$dictionary['idc_InformacionDeComercio']['fields']['year_c']['labelValue']='Año';

 
?>