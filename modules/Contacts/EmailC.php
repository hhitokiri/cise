<?php 

class Email {

     function sendMessage(&$bean, $event, $arguments){
          require_once('include/SugarPHPMailer.php');
          $welcome_msg = new SugarPHPMailer();

          //Grab the Lead's Status (only send if Status = New)
          // $status = $bean->status;
          if (date('Y-m-d H:i:s', $bean->date_entered) == date('Y-m-d H:i:s', $bean->date_modified)) {
              $status = 'New';
          }else {
               $status = 'Old';
          }
          //capturar el nombre del usuario para mandalo en la notificaccion
          $bean = BeanFactory::getBean('Users', $bean->created_by);


          //datos del usuario enlace y del admin para mandarle los datos por el formulario
          $administrador_datos = get_Admin();
          $usuario_datos_enlace = get_User_enlace('Users', $bean->created_by);

          // //Set the TO name and e-mail address for the message
          // // $rcpt_name = $bean->first_name . ' ' . $bean->last_name;
          $rcpt_name = $usuario_datos_enlace->first_name .' '.$usuario_datos_enlace->last_name ;
          $rcpt_name_admin = 'Admin';
          // // $rcpt_email = $bean->email1;
          $rcpt_email =  $administrador_datos->email1;
          $rcpt_email_cc = $usuario_datos_enlace->email1;

          // //Set RCPT address
          $welcome_msg->AddAddress($rcpt_email, $rcpt_name_admin);
          $welcome_msg->AddCC($rcpt_email_cc, $rcpt_name);

          //Setup template
          // require_once('XTemplate/xtpl.php');
          // $xtpl = new XTemplate('modules/q1w2e_probando/WelcomeMessage.html');
      
          //Check the status on record
          if ($status == 'New'){
               //Send welcome msg to Lead
               // $template_name = 'Lead';
               // echo "string";
               //Assign values to variables in template
               // $xtpl->assign('EMAIL_ADDY', $rcpt_email);
               // $xtpl->parse($template_name);
               $TEMPLATE_NOTIFICACION = '<h1>Sice</h1> <br>';
               $TEMPLATE_NOTIFICACION .= '<h4>Usuario</h4> <br>';
               $TEMPLATE_NOTIFICACION .= '<p>Descripcion</p> <br>';
               $TEMPLATE_NOTIFICACION .= '<p> objetos </p>';
               $TEMPLATE_NOTIFICACION .= '<p>direccion general</p>';
               $saludito = [1,2,3,4];
               foreach ($saludito as $key => $value) {
                    $salida .= '<a href='.$value.'>'.$value.'</a> <br>';
               }
               // echo $salida;
               // $welcome_msg->Body = from_html(trim($xtpl->text($template_name))); 
               $welcome_msg->ContentType = "text/html";
               $welcome_msg->Body = "<a href='http://test.com'>".$bean->first_name ." ".$bean->email1."</a> <br><h3>".date('Y-m-d H:i:s')."</h3> <h1> ".$bean->date_entered."</h1>".$salida."";
               $welcome_msg->Subject = 'Prueba de correo';
               $welcome_msg->prepForOutbound();
               $welcome_msg->setMailerForSystem();
               $welcome_msg->From = 'hikojuegos@gmail.com';
               $welcome_msg->FromName = 'Juaco';
               // $welcome_msg->Send();

               //Send the message, log if error occurs
               if (!$welcome_msg->Send())
               {
                    $GLOBALS['log']->fatal('Error sending e-mail: ' . $welcome_msg->ErrorInfo);
               }
          }
      }

     function get_Admin(){
          $record_id = 1;
          $module = 'Users';
          $bean = BeanFactory::getBean($module, $record_id);
          return  $bean;
     }
     function  get_User_enlace($module, $record_id){
          $bean = BeanFactory::getBean($module, $record_id);
          $bean_enlace = BeanFactory::getBean($module, $bean->enlace_sv_c);
          return  $bean_enlace;
     }

}

// $mensaje = new ViewEmailView();
// $mensaje->sendMessage();
//funcion para mandar emails por el modulo email

 // function sendEmail($ToEmailAdd, $FromEmailAdd, $FromEmailName, $EmailSubject, $EmailBody) {

 //    global $sugar_config;

 //    $GLOBALS['log']->debug('PREPARE EMAIL');

 //    require_once ('modules/Emails/Email.php');

 //    $To = explode(';',$ToEmailAdd);

 //    $now = gmdate("Y-m-d H:i:s");

 //    foreach ($To as $to_addr){

 //       $GLOBALS['log']->debug('PREPARE EMAIL TO:' . $to_addr);

 //       if (filter_var($to_addr, FILTER_VALIDATE_EMAIL)){

 //                $email = new Email();

 //                $email->to_addrs_arr = array(array('email' => $to_addr));

 //                $email->to_addrs = $to_addr;

 //                $email->from_addr = $FromEmailAdd;

 //                $email->from_name = $FromEmailName;

 //                //empty values are necessary to avoid undefined errors in Email.php

 //                $email->cc_addrs_arr = array();

 //                $email->bcc_addrs_arr = array();

 //                $email->saved_attachments = array();

 //                $email->name = $EmailSubject;

 //                $email->description_html = $EmailBody;

 //                $email->status = 'sent';

 //                $email->type = 'out';

 //                $email->date_sent = $now;

 //                $email->send();

 //                $email->save();

 //                $GLOBALS['log']->debug('EMAIL SENT:' . $email->id);

 //        }

 //    }

 //    return;

 //  }


//uso de bean beanfactory

// $user=BeanFactory::getBean('Users',$user_id); $primary_email=$user->emailAddress->getPrimaryAddress($user);
?>
