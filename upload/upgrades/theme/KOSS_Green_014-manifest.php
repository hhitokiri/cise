<?php
$manifest = array (
	'acceptable_sugar_versions' => array(
		'exact_matches' => array(),
		'regex_matches' => array(
			0 => '6\.*\.*',
		),
	),
	'acceptable_sugar_flavors' => array(
		0 => 'CE',
		1 => 'PRO',
	),
	'name' => 'KOSS_Green_Theme',
	'description' => 'KOSS Business Solutions Green Theme',
	'author' => 'KOSS Business Solutions',
	'published_date' => '2012-04-13 0:12:00',
	'version' => '6\.*\.*',
	'type' => 'theme',
	'is_uninstallable' => TRUE,
	'icon' => 'images/Themes.gif',
	'copy_files' => array(
		'from_dir' => 'KOSS_Green',
		'to_dir' => 'themes/KOSS_Green',
		'force_copy' => array(),
	),
);
?>
