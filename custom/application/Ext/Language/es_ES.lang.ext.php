<?php 
 //WARNING: The contents of this file are auto-generated


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['a123_seguimiento'] = 'seguimiento';
$app_list_strings['moduleList']['a123_Lineasdeproductos'] = 'Lineas de producto (C.Comerciales)';
$app_list_strings['moduleList']['a123_ReferenciasComerciales'] = 'Referencias Comerciales';
$app_list_strings['moduleList']['a123_Referenciascomerciales'] = 'Referencias Comerciales (C.c)';
$app_list_strings['moduleList']['a123_ContactoEnLaEmpresa'] = 'Contacto En la Empresa (C.I)';
$app_list_strings['moduleList']['a123_pais'] = 'Pais';
$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Existing Business'] = 'Negocios Existentes';
$app_list_strings['_type_dom']['New Business'] = 'Nuevos Negocios';
$app_list_strings['tip_pro_list1'][1] = 'Secos';
$app_list_strings['tip_pro_list1'][2] = 'Refrigerados';
$app_list_strings['tip_pro_list'][1] = 'Secos';
$app_list_strings['tip_pro_list'][2] = 'Refrigerados';
$app_list_strings['tip_proo_0'][1] = 'Secos';
$app_list_strings['tip_proo_0'][2] = 'Refrigerados';
$app_list_strings['contac_gener_list1'][1] = 'Campaña de promocion';
$app_list_strings['contac_gener_list1'][2] = 'Contacto Personal';
$app_list_strings['contac_gener_list1'][3] = 'Evento';
$app_list_strings['contac_gener_list1'][4] = 'Feria';
$app_list_strings['contac_gener_list1'][5] = 'Inversionista Establecido';
$app_list_strings['contac_gener_list1'][6] = 'Llamada Telefonica o Email';
$app_list_strings['contac_gener_list1'][7] = 'Referencia';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['fin_calculofinanciero'] = 'Calculo Financiero';
$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Existing Business'] = 'Negocios Existentes';
$app_list_strings['_type_dom']['New Business'] = 'Nuevos Negocios';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['q1w2e_probando'] = 'probando';
$app_list_strings['moduleList']['q1w2e_probando2'] = 'probando2';
$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Existing Business'] = 'Negocios Existentes';
$app_list_strings['_type_dom']['New Business'] = 'Nuevos Negocios';
$app_list_strings['_type_dom']['Administration'] = 'Administración';
$app_list_strings['_type_dom']['Product'] = 'Producto';
$app_list_strings['_type_dom']['User'] = 'Usuario';
$app_list_strings['_status_dom']['New'] = 'Nuevo';
$app_list_strings['_status_dom']['Assigned'] = 'Asignado';
$app_list_strings['_status_dom']['Closed'] = 'Cerrado';
$app_list_strings['_status_dom']['Pending Input'] = 'Pendiente de Información';
$app_list_strings['_status_dom']['Rejected'] = 'Rechazado';
$app_list_strings['_status_dom']['Duplicate'] = 'Duplicado';
$app_list_strings['_priority_dom']['P1'] = 'Alta';
$app_list_strings['_priority_dom']['P2'] = 'Media';
$app_list_strings['_priority_dom']['P3'] = 'Baja';
$app_list_strings['_resolution_dom'][''] = '';
$app_list_strings['_resolution_dom']['Accepted'] = 'Aceptado';
$app_list_strings['_resolution_dom']['Duplicate'] = 'Duplicado';
$app_list_strings['_resolution_dom']['Closed'] = 'Cerrado';
$app_list_strings['_resolution_dom']['Out of Date'] = 'Caducado';
$app_list_strings['_resolution_dom']['Invalid'] = 'No Válido';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['Cal_calculo'] = 'calculo';
$app_list_strings['cal_calculo_category_dom'][''] = '';
$app_list_strings['cal_calculo_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['cal_calculo_category_dom']['Knowledege Base'] = 'Knowledge Base';
$app_list_strings['cal_calculo_category_dom']['Sales'] = 'Sales';
$app_list_strings['cal_calculo_subcategory_dom'][''] = '';
$app_list_strings['cal_calculo_subcategory_dom']['Marketing Collateral'] = 'Marketing Collateral';
$app_list_strings['cal_calculo_subcategory_dom']['Product Brochures'] = 'Product Brochures';
$app_list_strings['cal_calculo_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['cal_calculo_status_dom']['Active'] = 'Active';
$app_list_strings['cal_calculo_status_dom']['Draft'] = 'Draft';
$app_list_strings['cal_calculo_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['cal_calculo_status_dom']['Expired'] = 'Expired';
$app_list_strings['cal_calculo_status_dom']['Under Review'] = 'Under Review';
$app_list_strings['cal_calculo_status_dom']['Pending'] = 'Pending';
$app_list_strings['_category_dom'][''] = '';
$app_list_strings['_category_dom']['Marketing'] = 'Marketing';
$app_list_strings['_category_dom']['Knowledege Base'] = 'Base de Conocimiento';
$app_list_strings['_category_dom']['Sales'] = 'Ventas';
$app_list_strings['_subcategory_dom'][''] = '';
$app_list_strings['_subcategory_dom']['Marketing Collateral'] = 'Impresos de Marketing';
$app_list_strings['_subcategory_dom']['Product Brochures'] = 'Folletos de Producto';
$app_list_strings['_subcategory_dom']['FAQ'] = 'FAQ';
$app_list_strings['_status_dom']['Active'] = 'Activo';
$app_list_strings['_status_dom']['Draft'] = 'Borrador';
$app_list_strings['_status_dom']['FAQ'] = 'FAQ';
$app_list_strings['_status_dom']['Expired'] = 'Caducado';
$app_list_strings['_status_dom']['Under Review'] = 'En Revisión';
$app_list_strings['_status_dom']['Pending'] = 'Pendiente';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['Rcc_ReferenciasC_empresas_compra'] = 'Referencias Comerciales (EmpC)';
$app_list_strings['_type_dom'][''] = '';
$app_list_strings['_type_dom']['Analyst'] = NULL;
$app_list_strings['_type_dom']['Competitor'] = NULL;
$app_list_strings['_type_dom']['Customer'] = NULL;
$app_list_strings['_type_dom']['Integrator'] = NULL;
$app_list_strings['_type_dom']['Investor'] = 'Inversor';
$app_list_strings['_type_dom']['Partner'] = 'Partner';
$app_list_strings['_type_dom']['Press'] = 'Prensa';
$app_list_strings['_type_dom']['Prospect'] = 'Prospecto';
$app_list_strings['_type_dom']['Reseller'] = 'Revendedor';
$app_list_strings['_type_dom']['Other'] = 'Otro';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['idc_InformacionDeComercio'] = 'Información de comercio';

?>