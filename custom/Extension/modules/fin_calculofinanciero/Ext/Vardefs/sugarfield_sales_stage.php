<?php
 // created: 2015-04-16 15:05:39
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['default']='Prospecting';
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['required']=false;
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['merge_filter']='disabled';

 ?>