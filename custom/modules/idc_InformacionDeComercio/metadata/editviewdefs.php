<?php
$module_name = 'idc_InformacionDeComercio';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
//              'includes' => 
//        array (
//          0 => array ('file' => 'modules/idc_InformacionDeComercio/mio.js'),
//        ),      
    ),
      
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'monto_ventasnac_c',
            'label' => 'LBL_MONTO_VENTASNAC',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'monto_expor_c',
            'label' => 'LBL_MONTO_EXPOR',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'monto_import_c',
            'label' => 'LBL_MONTO_IMPORT',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'destino_expor_c',
            'label' => 'LBL_DESTINO_EXPOR',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'origen_import_c',
            'label' => 'LBL_ORIGEN_IMPORT',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'exp_eventos_c',
            'label' => 'LBL_EXP_EVENTOS',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'conq_entidad_c',
            'label' => 'LBL_CONQ_ENTIDAD',
          ),
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'year_c',
            'studio' => 'visible',
            'label' => 'LBL_YEAR',
          ),
        ),
      ),
    ),
  ),
);
?>
