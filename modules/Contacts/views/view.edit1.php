<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


class ContactsViewEdit extends ViewEdit
{
 	public function __construct()
 	{
 		parent::ViewEdit();
 		$this->useForSubpanel = true;
 		$this->useModuleQuickCreateTemplate = true;
 	}

 	/**
 	 * @see SugarView::display()
	 *
 	 * We are overridding the display method to manipulate the sectionPanels.
 	 * If portal is not enabled then don't show the Portal Information panel.
 	 */
 	public function display()
 	{
        
                 
        $this->ev->process();
		if ( !empty($_REQUEST['contact_name']) && !empty($_REQUEST['contact_id'])
            && $this->ev->fieldDefs['report_to_name']['value'] == ''
            && $this->ev->fieldDefs['reports_to_id']['value'] == '') {
            $this->ev->fieldDefs['report_to_name']['value'] = $_REQUEST['contact_name'];
            $this->ev->fieldDefs['reports_to_id']['value'] = $_REQUEST['contact_id'];
        }
        $admin = new Administration();
		$admin->retrieveSettings();
		if(empty($admin->settings['portal_on']) || !$admin->settings['portal_on']) {
		   unset($this->ev->sectionPanels[strtoupper('lbl_portal_information')]);
		} else {
           if (isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true' ) {
               $this->ev->fieldDefs['portal_name']['value'] = '';
               $this->ev->fieldDefs['portal_active']['value'] = '0';
               $this->ev->fieldDefs['portal_password']['value'] = '';
               $this->ev->fieldDefs['portal_password1']['value'] = '';
               $this->ev->fieldDefs['portal_name_verified'] = '0';
               $this->ev->focus->portal_name = '';
               $this->ev->focus->portal_password = '';
               $this->ev->focus->portal_acitve = 0;
           }
           else {
               $this->ev->fieldDefs['portal_password']['value'] = '';
               $this->ev->fieldDefs['portal_password1']['value'] = '';               
           }
		   echo getVersionedScript('modules/Contacts/Contact.js');
		   echo '<script language="javascript">';
		   echo 'addToValidateComparison(\'EditView\', \'portal_password\', \'varchar\', false, SUGAR.language.get(\'app_strings\', \'ERR_SQS_NO_MATCH_FIELD\') + SUGAR.language.get(\'Contacts\', \'LBL_PORTAL_PASSWORD\'), \'portal_password1\');';
           echo 'addToValidateVerified(\'EditView\', \'portal_name_verified\', \'bool\', false, SUGAR.language.get(\'app_strings\', \'ERR_EXISTING_PORTAL_USERNAME\'));';
           echo 'YAHOO.util.Event.onDOMReady(function() {YAHOO.util.Event.on(\'portal_name\', \'blur\', validatePortalName);YAHOO.util.Event.on(\'portal_name\', \'keydown\', handleKeyDown);});';
		   echo '</script>';
	}
              global $mod_strings; 
  echo <<<EOHTML
<script>
$(document).ready(function () {
        
var select = $('select[name="tipo_contac_c"]');//definimos el id del select tipo de contacto
var b = $("#tipo_contac_c option:checked").val();// Captura el value de select tipo de contacto

var c =['3','5','6','7','8','9'];//Comercio: > Oculta todos los campos que no tienen relacion con comercio
var d =['3','6','7','8','9'];//Turismo:  > Oculta todos los campos que no tienen relacion con turismo    
var e =['3','5','6','7','8','9'];// - > Oculta todos los campos y solo visualiza la base
var f =['3','6','7','8','9'];// - > Oculta todos los campos y solo visualiza formularios base (empresa y datos de contacto)
var g =['10','11','12'];// ocultar o mostrar gremiales, medios especializadps, oportu comerciales
              
////////////////////////// Mostar y esconder paneles segun el tipo de contacto al momento de editar

        if(b === "0"){// -
            e.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});

		});
                }else if(b === "1"){ // Comercio
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});
                }else if(b === "2"){ // Turismo
                    $('#detailpanel_5').show();
                    d.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();                 
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});
                }else if(b === "3"){ // inversion
                    $('#razon_soc_c').show();
                    $('#razon_soc_c_label').show();                
                    $('#detailpanel_5').hide();
                    $('#detailpanel_10').hide();
                    f.forEach(function(entry) {
		    $('#detailpanel_'+entry).show();
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});

                }else if(b === "4"){ //Camaras y gremiales
                    $('#detailpanel_10').show();
                    f.forEach(function(entry) {
		    $('#detailpanel_'+entry).show();
                    });
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
                }else if(b === "5"){//Medios especializados
                    $('#detailpanel_11').show();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    
                }else if(b === "6"){//Oportunidades comerciales
                    $('#detailpanel_12').show();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    
                }
 
///// cuando cambia el select onChange.
        select.on('change',function(){
        var a= $("#tipo_contac_c option:checked").val();
        if(a === "0"){
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();
                    e.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
   
        } else if (a ==="1") {
            //COMERCIO
                
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
                
        } else if (a ==="2") {
            //TURISMO
                    $('#detailpanel_5').show();
                    d.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();                 
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    

        } else if (a === "3") {
            //INVERSION
                    $('#razon_soc_c').show();
                    $('#razon_soc_c_label').show();                
                    $('#detailpanel_5').hide();
                    $('#detailpanel_10').hide();
                    $('#detailpanel_11').hide();
                    $('#detailpanel_12').hide();                    
                    f.forEach(function(entry) {
		    $('#detailpanel_'+entry).show();
                    });
                    g.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    
        } else if (a === "4"){//camaras y gremiales
                    $('#detailpanel_5').hide();
                    $('#detailpanel_10').show();
                    $('#detailpanel_11').hide();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();                       
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });
        } else if(a === "5"){// medios especializados
                    $('#detailpanel_11').show();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();                     
                    $('#detailpanel_10').hide();
                    $('#detailpanel_12').hide();
                    $('#detailpanel_5').hide();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    
        } else{//Oportunidades comerciales
                    $('#detailpanel_12').show();
                    $('#razon_soc_c').hide();
                    $('#razon_soc_c_label').hide();                     
                    $('#detailpanel_11').hide();
                    $('#detailpanel_10').hide();
                    c.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                    });                    
                }              
                });
              
              
              
//////////////// Fin ///////////////////////////

/////////////////////////////////////////////// Mostar y esconder campos depeniendo si la empresa dio o no informacion

        
       function elem_makeReadOnly(id) {//El elemento esta inhabilitado   
            var obj = document.getElementById(id);     
            if (obj != null) {obj.readOnly = true;obj.style.backgroundColor = "lightgray";}
            return obj; }
        
       var info_emp1 = $('select[name="info_emp_c"]');//definimos el id del select de informacion de la empresa
       var aa = $('#info_emp_c').val();//capturamos el value de select informacion de la empresa
        if(aa === '1'){//si
            Inversion();

        }else if(aa === '2'){//no
                var campos2=['nombre_emp_c','telefono_emp_c','act_emp_c','direccion_emp_c','fax_emp_c','web_emp_c','facebook_emp_c','twitter_emp_c']; 
                campos2.forEach(function(entry){
                elem_makeReadOnly(""+entry+"");
            });  
        }
        
  
        info_emp1.on('change',function(){ 
        var info_emp2 = $("#info_emp_c option:checked").val();// Captura el value de select informacion de la empresa  
        var campos=['nombre_emp_c','telefono_emp_c','act_emp_c','direccion_emp_c','fax_emp_c','web_emp_c','facebook_emp_c','twitter_emp_c']; 

         if(info_emp2 === "1"){//Si dio informacion la empresa
            campos.forEach(function(entry){
                removeReadonly(""+entry+"");
            });
            Inversion();

        } else if(info_emp2 === "2") {//No dio informacion la empresa
 
            campos.forEach(function(entry){
                elem_makeReadOnly(""+entry+"");
            });
            removeReadonly(facebook_emp_c);
        var removerdatosrqueridosEmpresa =  [{'objeto':'nombre_emp_c','label':'LBL_NOMBRE_EMP'},
                     {'objeto':'telefono_emp_c','label':'LBL_TELEFONO_EMP'},
                     {'objeto':'act_emp_c','label':'LBL_ACT_EMP'},
                     {'objeto':'direccion_emp_c','label':'LBL_DIRECCION_EMP'},
                     {'objeto':'fax_emp_c','label':'LBL_FAX_EMP'},
                     {'objeto':'web_emp_c','label':'LBL_WEB_EMP'},
                     {'objeto':'info_emp_c','label':'LBL_INFO_EMP'},
                    ];
            removerdatosrqueridosEmpresa.forEach(function(entry){            
            removeFromValidate('EditView',entry.objeto);                        // else remove the validtion applied
            $(""+entry.label+"").html('{$mod_strings['entry.label']}: ');
            });     
        }
        
        function elem_makeReadOnly(id) {//El elemento esta inhabilitado   
            var obj = document.getElementById(id);     
            if (obj != null) {obj.readOnly = true;obj.style.backgroundColor = "lightgray";}
            return obj; }
        
        function removeReadonly(id){ //EL elemento esta habilitado  
            var obj = document.getElementById(id);
            if (obj != null) {obj.readOnly = false;obj.style.backgroundColor = "white";}
            return obj;            
        }
    });          
  ///////////////////////FIN            
              
                     function Inversion()
                     {
                        var b=['nombre_emp_c','telefono_emp_c','act_emp_c','direccion_emp_c','fax_emp_c','web_emp_c_label','info_emp_c'];                                  
                        var datosEmpresa =  [{'objeto':'nombre_emp_c','label':'LBL_NOMBRE_EMP'},
                                             {'objeto':'telefono_emp_c','label':'LBL_TELEFONO_EMP'},
                                             {'objeto':'act_emp_c','label':'LBL_ACT_EMP'},
                                             {'objeto':'direccion_emp_c','label':'LBL_DIRECCION_EMP'},
                                             {'objeto':'fax_emp_c','label':'LBL_FAX_EMP'},
                                             {'objeto':'web_emp_c','label':'LBL_WEB_EMP'},
                                             {'objeto':'info_emp_c','label':'LBL_INFO_EMP'},
                                            ];
                                     
                        var status = $('b').val(); // get current value of the field 
                        
                         if(status == ''){ // check if it matches the condition: if true,
                                
                                datosEmpresa.forEach(function(entry){
                                addToValidate('EditView',entry.objeto,'varchar',true,'{$mod_strings['entry.label']}');
                                $(""+entry.label+"").html('{$mod_strings['entry.label']} <font color="red">*</font>');
                                });                            
                            }
                            
//                           if(status == ''){ // check if it matches the condition: if true,
//                                addToValidate('EditView','phone_work','varchar',true,'{$mod_strings['LBL_OFFICE_PHONE']}');    // mark Description field required
//                                $('#phone_work_label').html('{$mod_strings['LBL_OFFICE_PHONE']} <font color="red">*</font>'); // with red * sign next to label
//                                
//                                addToValidate('EditView','description','varchar',true,'{$mod_strings['LBL_DESCRIPTION']}');    // mark Description field required
//                                $('#description_label').html('{$mod_strings['LBL_DESCRIPTION']} <font color="red">*</font>'); // with red * sign next to label                                
//                                
//                                addToValidate('EditView','first_name','varchar',true,'{$mod_strings['LBL_FIRST_NAME']}');    // mark Description field required
//                                $('#first_name_label').html('{$mod_strings['LBL_FIRST_NAME']} <font color="red">*</font>'); // with red * sign next to label                                
//                            }
//                            
                            
                            
                            
                            else{
//                                 
//                                datosEmpresa.forEach(function(entry)){
//                                removeFromValidate('EditView',""+entry.objeto+"");                        // else remove the validtion applied
//                                $(""+entry.id+"").html('{$mod_strings['entry.label']}: '); // and give the normal label back 
//                                });       
                                removeFromValidate('EditView','telefono_emp_c');                        // else remove the validtion applied
                                $('#telefono_emp_c_label').html('{$mod_strings['LBL_FIRST_NAME']}: '); // and give the normal label back 
                            }
                    }
                    //Call at onload while editing a Published blog record

});                      
</script>
EOHTML;

		echo $this->ev->display($this->showTitle);
	
 	}
}