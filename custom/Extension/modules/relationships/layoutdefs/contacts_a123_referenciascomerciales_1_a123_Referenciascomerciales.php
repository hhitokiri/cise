<?php
 // created: 2015-03-03 13:38:58
$layout_defs["a123_Referenciascomerciales"]["subpanel_setup"]['contacts_a123_referenciascomerciales_1'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'contacts_a123_referenciascomerciales_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
