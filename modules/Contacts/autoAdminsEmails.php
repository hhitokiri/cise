<?php


class EmailsAdmin {
      function updateEmailsAdmin(&$bean, $event, $arguments) 
      {
           //Query ACCOUNTS table for assigned_user_id value of parent Account
           $case_id = $bean->id;
           $acct_id = $bean->account_id;
           $query =  "SELECT accts.assigned_user_id FROM accounts accts ";
           $query .= "WHERE accts.id = '$acct_id' LIMIT 1";
           $results = $bean->db->query($query, true);
           $row = $bean->db->fetchByAssoc($results);
           $user_id = $row['assigned_user_id'];

           //Change assigned_user_id value on Case
           $bean->assigned_user_id = $user_id;
      }
}
?>