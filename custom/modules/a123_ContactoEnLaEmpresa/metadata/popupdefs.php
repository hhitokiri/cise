<?php
$popupMeta = array (
    'moduleMain' => 'a123_ContactoEnLaEmpresa',
    'varName' => 'a123_ContactoEnLaEmpresa',
    'orderBy' => 'a123_contactoenlaempresa.name',
    'whereClauses' => array (
  'name' => 'a123_contactoenlaempresa.name',
),
    'searchInputs' => array (
  0 => 'a123_contactoenlaempresa_number',
  1 => 'name',
  2 => 'priority',
  3 => 'status',
),
    'listviewdefs' => array (
  'NOMBRE_CONTAC' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NOMBRE_CONTAC',
    'width' => '10%',
    'default' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
    'name' => 'name',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
    'name' => 'assigned_user_name',
  ),
),
);
