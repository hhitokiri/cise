<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2015-02-16 08:58:28
$dictionary["a123_ContactoEnLaEmpresa"]["fields"]["a123_contactoenlaempresa_contacts_1"] = array (
  'name' => 'a123_contactoenlaempresa_contacts_1',
  'type' => 'link',
  'relationship' => 'a123_contactoenlaempresa_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_CONTACTS_TITLE',
);


 // created: 2015-02-17 10:37:07
$dictionary['a123_ContactoEnLaEmpresa']['fields']['idioma_b_c']['labelValue']='Idioma';

 

 // created: 2015-03-04 11:24:05
$dictionary['a123_ContactoEnLaEmpresa']['fields']['name']['required']=true;
$dictionary['a123_ContactoEnLaEmpresa']['fields']['name']['duplicate_merge']='disabled';
$dictionary['a123_ContactoEnLaEmpresa']['fields']['name']['duplicate_merge_dom_value']='0';
$dictionary['a123_ContactoEnLaEmpresa']['fields']['name']['merge_filter']='disabled';
$dictionary['a123_ContactoEnLaEmpresa']['fields']['name']['unified_search']=false;

 

 // created: 2015-02-16 09:06:16
$dictionary['a123_ContactoEnLaEmpresa']['fields']['conta_gene_otro_c']['labelValue']='Otro (Especifique)';

 

 // created: 2015-03-04 11:19:23
$dictionary['a123_ContactoEnLaEmpresa']['fields']['nombre_contac']['required']=false;

 

 // created: 2015-02-16 09:13:53
$dictionary['a123_ContactoEnLaEmpresa']['fields']['contacto_generado_c']['labelValue']='Contacto Generad';

 
?>