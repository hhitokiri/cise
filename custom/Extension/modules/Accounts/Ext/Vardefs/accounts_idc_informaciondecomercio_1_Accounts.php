<?php
// created: 2015-03-12 12:35:05
$dictionary["Account"]["fields"]["accounts_idc_informaciondecomercio_1"] = array (
  'name' => 'accounts_idc_informaciondecomercio_1',
  'type' => 'link',
  'relationship' => 'accounts_idc_informaciondecomercio_1',
  'source' => 'non-db',
  'module' => 'idc_InformacionDeComercio',
  'bean_name' => 'idc_InformacionDeComercio',
  'vname' => 'LBL_ACCOUNTS_IDC_INFORMACIONDECOMERCIO_1_FROM_IDC_INFORMACIONDECOMERCIO_TITLE',
);
