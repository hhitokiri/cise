<?php
 // created: 2015-03-12 12:35:05
$layout_defs["Accounts"]["subpanel_setup"]['accounts_idc_informaciondecomercio_1'] = array (
  'order' => 100,
  'module' => 'idc_InformacionDeComercio',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_IDC_INFORMACIONDECOMERCIO_1_FROM_IDC_INFORMACIONDECOMERCIO_TITLE',
  'get_subpanel_data' => 'accounts_idc_informaciondecomercio_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
