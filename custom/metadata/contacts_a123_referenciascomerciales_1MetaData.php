<?php
// created: 2015-03-03 13:38:58
$dictionary["contacts_a123_referenciascomerciales_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'contacts_a123_referenciascomerciales_1' => 
    array (
      'lhs_module' => 'Contacts',
      'lhs_table' => 'contacts',
      'lhs_key' => 'id',
      'rhs_module' => 'a123_Referenciascomerciales',
      'rhs_table' => 'a123_referenciascomerciales',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'contacts_a123_referenciascomerciales_1_c',
      'join_key_lhs' => 'contacts_a123_referenciascomerciales_1contacts_ida',
      'join_key_rhs' => 'contacts_a95d3rciales_idb',
    ),
  ),
  'table' => 'contacts_a123_referenciascomerciales_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'contacts_a123_referenciascomerciales_1contacts_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'contacts_a95d3rciales_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'contacts_a123_referenciascomerciales_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'contacts_a123_referenciascomerciales_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'contacts_a123_referenciascomerciales_1contacts_ida',
        1 => 'contacts_a95d3rciales_idb',
      ),
    ),
  ),
);