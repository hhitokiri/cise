<?php
// created: 2015-03-03 15:11:39
$subpanel_layout['list_fields'] = array (
  'name' => 
  array (
    'vname' => 'LBL_NAME',
    'widget_class' => 'SubPanelDetailViewLink',
    'width' => '45%',
    'default' => true,
  ),
  'pais_c' => 
  array (
    'type' => 'enum',
    'default' => true,
    'studio' => 'visible',
    'vname' => 'LBL_PAIS',
    'width' => '10%',
  ),
  'nombre_contacto_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_NOMBRE_CONTACTO',
    'width' => '10%',
  ),
  'cargo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CARGO',
    'width' => '10%',
  ),
  'phone_office' => 
  array (
    'type' => 'phone',
    'vname' => 'LBL_PHONE_OFFICE',
    'width' => '10%',
    'default' => true,
  ),
  'correo_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_CORREO',
    'width' => '10%',
  ),
  'assigned_user_name' => 
  array (
    'name' => 'assigned_user_name',
    'vname' => 'LBL_ASSIGNED_USER',
    'widget_class' => 'SubPanelDetailViewLink',
    'target_record_key' => 'assigned_user_id',
    'target_module' => 'Employees',
    'width' => '10%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'Rcc_ReferenciasC_empresas_compra',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'Rcc_ReferenciasC_empresas_compra',
    'width' => '5%',
    'default' => true,
  ),
);