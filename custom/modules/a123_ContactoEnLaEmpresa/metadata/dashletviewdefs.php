<?php
$dashletData['a123_ContactoEnLaEmpresaDashlet']['searchFields'] = array (
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'type' => 'assigned_user_name',
    'default' => 'Administrator',
  ),
);
$dashletData['a123_ContactoEnLaEmpresaDashlet']['columns'] = array (
  'nombre_contac' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NOMBRE_CONTAC',
    'width' => '10%',
    'default' => true,
    'name' => 'nombre_contac',
  ),
  'name' => 
  array (
    'width' => '40%',
    'label' => 'LBL_LIST_NAME',
    'link' => true,
    'default' => true,
    'name' => 'name',
  ),
  'date_entered' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_ENTERED',
    'default' => true,
    'name' => 'date_entered',
  ),
  'cargo_contac' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CARGO_CONTAC',
    'width' => '10%',
    'default' => false,
    'name' => 'cargo_contac',
  ),
  'date_modified' => 
  array (
    'width' => '15%',
    'label' => 'LBL_DATE_MODIFIED',
    'name' => 'date_modified',
    'default' => false,
  ),
  'created_by' => 
  array (
    'width' => '8%',
    'label' => 'LBL_CREATED',
    'name' => 'created_by',
    'default' => false,
  ),
  'assigned_user_name' => 
  array (
    'width' => '8%',
    'label' => 'LBL_LIST_ASSIGNED_USER',
    'name' => 'assigned_user_name',
    'default' => false,
  ),
);
