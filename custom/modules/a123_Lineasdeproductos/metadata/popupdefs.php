<?php
$popupMeta = array (
    'moduleMain' => 'a123_Lineasdeproductos',
    'varName' => 'a123_Lineasdeproductos',
    'orderBy' => 'a123_lineasdeproductos.name',
    'whereClauses' => array (
  'description' => 'a123_lineasdeproductos.description',
),
    'searchInputs' => array (
  4 => 'description',
),
    'searchdefs' => array (
  'description' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'name' => 'description',
  ),
),
);
