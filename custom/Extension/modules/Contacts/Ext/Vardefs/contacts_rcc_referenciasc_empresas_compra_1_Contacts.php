<?php
// created: 2015-02-27 08:54:27
$dictionary["Contact"]["fields"]["contacts_rcc_referenciasc_empresas_compra_1"] = array (
  'name' => 'contacts_rcc_referenciasc_empresas_compra_1',
  'type' => 'link',
  'relationship' => 'contacts_rcc_referenciasc_empresas_compra_1',
  'source' => 'non-db',
  'module' => 'Rcc_ReferenciasC_empresas_compra',
  'bean_name' => 'Rcc_ReferenciasC_empresas_compra',
  'vname' => 'LBL_CONTACTS_RCC_REFERENCIASC_EMPRESAS_COMPRA_1_FROM_RCC_REFERENCIASC_EMPRESAS_COMPRA_TITLE',
);
