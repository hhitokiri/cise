<?php
// created: 2015-04-23 11:00:13
$mod_strings = array (
  'LBL_DESCRIPTION' => 'Description del Producto:',
  'LBL_OFFICE_PHONE' => 'Phone:',
  'LBL_TELEFONO_EMP' => 'Phone:',
  'LBL_CONTACTS_RCC_REFERENCIASC_EMPRESAS_COMPRA_1_FROM_RCC_REFERENCIASC_EMPRESAS_COMPRA_TITLE' => 'Referencias Comerciales (Empresas a las que compra)',
  'LBL_CONTACTS_A123_REFERENCIASCOMERCIALES_1_FROM_A123_REFERENCIASCOMERCIALES_TITLE' => 'Trade References',
  'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_A123_CONTACTOENLAEMPRESA_TITLE' => 'Contact The Company (C.I)',
  'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_A123_LINEASDEPRODUCTOS_TITLE' => 'Product lines (C.Comerciales)',
);