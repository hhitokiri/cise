<?php
 // created: 2015-02-18 10:16:37
$dictionary['Contact']['fields']['phone_work']['required']=true;
$dictionary['Contact']['fields']['phone_work']['comments']='Work phone number of the contact';
$dictionary['Contact']['fields']['phone_work']['merge_filter']='disabled';
$dictionary['Contact']['fields']['phone_work']['audited']=false;

 ?>