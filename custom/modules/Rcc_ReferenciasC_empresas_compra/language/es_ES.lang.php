<?php
// created: 2015-03-03 14:04:23
$mod_strings = array (
  'LBL_CARGO' => 'Cargo',
  'LBL_PAIS' => 'Pais',
  'LBL_QUICKCREATE_PANEL1' => 'Nombre de contacto',
  'LBL_NAME' => 'Nombre de la empresa',
  'LBL_NOMBRE_CONTACTO' => 'Nombre de contacto',
  'LBL_PHONE_OFFICE' => 'Teléfono',
  'LBL_CORREO' => 'Email',
);