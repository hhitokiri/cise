<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-04-16 15:06:24
$dictionary['fin_calculofinanciero']['fields']['name']['required']=true;
$dictionary['fin_calculofinanciero']['fields']['name']['comments']='Name of the Sale';
$dictionary['fin_calculofinanciero']['fields']['name']['merge_filter']='disabled';
$dictionary['fin_calculofinanciero']['fields']['name']['unified_search']=false;

 

 // created: 2015-04-16 15:05:54
$dictionary['fin_calculofinanciero']['fields']['date_closed']['required']=false;
$dictionary['fin_calculofinanciero']['fields']['date_closed']['comments']='Expected or actual date the sale will close';
$dictionary['fin_calculofinanciero']['fields']['date_closed']['merge_filter']='disabled';

 

 // created: 2015-04-16 15:05:39
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['default']='Prospecting';
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['required']=false;
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['comments']='Indication of progression towards closure';
$dictionary['fin_calculofinanciero']['fields']['sales_stage']['merge_filter']='disabled';

 
?>