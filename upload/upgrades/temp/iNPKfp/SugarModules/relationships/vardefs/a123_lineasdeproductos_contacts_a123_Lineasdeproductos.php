<?php
// created: 2015-02-16 08:41:20
$dictionary["a123_Lineasdeproductos"]["fields"]["a123_lineasdeproductos_contacts"] = array (
  'name' => 'a123_lineasdeproductos_contacts',
  'type' => 'link',
  'relationship' => 'a123_lineasdeproductos_contacts',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_CONTACTS_TITLE',
);
