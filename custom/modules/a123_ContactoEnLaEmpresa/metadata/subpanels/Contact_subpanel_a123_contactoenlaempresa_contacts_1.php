<?php
// created: 2015-02-24 10:25:09
$subpanel_layout['list_fields'] = array (
  'nombre_contac' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_NOMBRE_CONTAC',
    'width' => '10%',
    'default' => true,
  ),
  'cargo_contac' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_CARGO_CONTAC',
    'width' => '10%',
    'default' => true,
  ),
  'tel_contac' => 
  array (
    'type' => 'varchar',
    'vname' => 'LBL_TEL_CONTAC',
    'width' => '10%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'a123_ContactoEnLaEmpresa',
    'width' => '4%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'a123_ContactoEnLaEmpresa',
    'width' => '5%',
    'default' => true,
  ),
);