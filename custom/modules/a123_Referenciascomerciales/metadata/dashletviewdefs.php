<?php
$dashletData['a123_ReferenciascomercialesDashlet']['searchFields'] = array (
  'nombre_contactv' => 
  array (
    'default' => '',
  ),
  'date_entered' => 
  array (
    'default' => '',
  ),
  'date_modified' => 
  array (
    'default' => '',
  ),
  'assigned_user_id' => 
  array (
    'default' => '',
  ),
);
$dashletData['a123_ReferenciascomercialesDashlet']['columns'] = array (
  'nombre_contactv' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NOMBRE_CONTACTV',
    'width' => '10%',
    'default' => true,
    'name' => 'nombre_contactv',
  ),
);
