<?php
// created: 2015-04-23 11:00:23
$mod_strings = array (
  'LBL_CARGO_CONTAC' => 'Cargo en la empresa',
  'LBL_CONTA_GENE_OTRO' => 'Otro (Especifique)',
  'LBL_CONTACTO_GENERADO' => 'Contacto generado en',
  'LBL_IDIOMA_B' => 'Idiomas dominados por le contacto',
  'LBL_TEL_CONTAC' => 'Telefono',
  'LBL_NAME' => 'Name',
  'LBL_NOMBRE_CONTAC' => 'Nombre de Contacto',
  'LBL_HOMEPAGE_TITLE' => 'Mi Contacto En la Empresa (C.I)',
);