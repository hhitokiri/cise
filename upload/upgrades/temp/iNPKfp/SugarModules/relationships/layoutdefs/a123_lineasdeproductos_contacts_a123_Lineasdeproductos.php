<?php
 // created: 2015-02-16 08:41:20
$layout_defs["a123_Lineasdeproductos"]["subpanel_setup"]['a123_lineasdeproductos_contacts'] = array (
  'order' => 100,
  'module' => 'Contacts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A123_LINEASDEPRODUCTOS_CONTACTS_FROM_CONTACTS_TITLE',
  'get_subpanel_data' => 'a123_lineasdeproductos_contacts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
