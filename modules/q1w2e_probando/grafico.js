$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Tabla de medición por Partes '
        },
        subtitle: {
            text: 'Source: rree.gob.sv'
        },
        xAxis: {
            categories: ['0', ],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        
        credits: {
            enabled: false
        },
       series: [{
                type: 'bar',
                name: 'Potenciales Inversionistas',
                composition: {
                    'Total %': '8%',
                    'Total ': '10'
                },
                data: [2]
            }, {
                type: 'bar',
                name: 'Respuesta a Requerimientos',
                composition: {
                    'Total %': '4%',
                    'Total ': '10'
                },
                data: [6]
            }, {
                type: 'bar',
                name: 'Oportunidades Comerciales ',
                color: 'red',
                composition: {
                    'Total %': '8%',
                    'Total ': '15'
                    
                },
                data: [9]
            }],

        tooltip: {
            shared: false,
            formatter: function() {
                var serie = this.series;
                var s = '<b>' + Highcharts.dateFormat('%A, %b %e, %Y', this.x) + '</b><br>';
                s += '<span style=color:' + serie.color + '>' + serie.options.name + '</span>: <b>' + this.y + '</b><br/>';
                $.each(serie.options.composition, function(name, value) {
                    s += '<b>' + name + ':</b> ' + value + '<br>';
                });
                return s;
            }
        }
    });
});