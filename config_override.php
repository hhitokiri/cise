<?php
/***CONFIGURATOR***/
$sugar_config['default_module_favicon'] = false;
$sugar_config['dashlet_auto_refresh_min'] = '30';
$sugar_config['enable_action_menu'] = true;
$sugar_config['stack_trace_errors'] = false;
$sugar_config['developerMode'] = false;
$sugar_config['disabled_languages'] = '';
$sugar_config['dbconfigoption']['collation'] = 'utf8_general_ci';
$sugar_config['default_language'] = 'es_ES';
$sugar_config['disabled_themes'] = '';
$sugar_config['default_theme'] = 'Sugar5';
$sugar_config['addAjaxBannedModules'][0] = 'q1w2e_probando';
/***CONFIGURATOR***/