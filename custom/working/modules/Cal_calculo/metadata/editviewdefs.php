<?php
$module_name = 'Cal_calculo';
$viewdefs [$module_name] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
        'hidden' => 
        array (
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{sugar_getscript file="include/javascript/popup_parent_helper.js"}
	{sugar_getscript file="cache/include/javascript/sugar_grp_jsolait.js"}
	{sugar_getscript file="modules/Documents/documents.js"}',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'paises',
            'studio' => 'visible',
            'label' => 'LBL_PAISES',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'potenciales_inversionistas',
            'label' => 'LBL_POTENCIALES_INVERSIONISTAS',
          ),
          1 => '',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'oportunidades_comerciales',
            'label' => 'LBL_OPORTUNIDADES_COMERCIALES',
          ),
          1 => 
          array (
            'name' => 'respuesta_a_requerimiento',
            'label' => 'LBL_RESPUESTA_A_REQUERIMIENTO',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'potenciales_inversionistas_n',
            'label' => 'LBL_POTENCIALES_INVERSIONISTAS_N',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'oportunidades_comerciales_n',
            'label' => 'LBL_OPORTUNIDADES_COMERCIALES_N',
          ),
          1 => 
          array (
            'name' => 'respuesta_a_requerimiento_n',
            'label' => 'LBL_RESPUESTA_A_REQUERIMIENTO_N',
          ),
        ),
      ),
    ),
  ),
);
?>
