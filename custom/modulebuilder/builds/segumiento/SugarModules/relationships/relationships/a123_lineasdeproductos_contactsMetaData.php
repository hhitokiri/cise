<?php
// created: 2015-02-16 08:54:25
$dictionary["a123_lineasdeproductos_contacts"] = array (
  'true_relationship_type' => 'many-to-many',
  'relationships' => 
  array (
    'a123_lineasdeproductos_contacts' => 
    array (
      'lhs_module' => 'a123_Lineasdeproductos',
      'lhs_table' => 'a123_lineasdeproductos',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'a123_lineasdeproductos_contacts_c',
      'join_key_lhs' => 'a123_lineasdeproductos_contactsa123_lineasdeproductos_ida',
      'join_key_rhs' => 'a123_lineasdeproductos_contactscontacts_idb',
    ),
  ),
  'table' => 'a123_lineasdeproductos_contacts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'a123_lineasdeproductos_contactsa123_lineasdeproductos_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'a123_lineasdeproductos_contactscontacts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'a123_lineasdeproductos_contactsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'a123_lineasdeproductos_contacts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'a123_lineasdeproductos_contactsa123_lineasdeproductos_ida',
        1 => 'a123_lineasdeproductos_contactscontacts_idb',
      ),
    ),
  ),
);