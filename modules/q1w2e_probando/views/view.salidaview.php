<?php 
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
require_once('include/MVC/View/views/view.detail.php');
/**
* 
*/
class ViewSalidaView extends ViewDetail{
	
	function ViewSalidaView(){
		parent::ViewDetail();
	}
	
	function display() {
		// $contactos = new Contacts();
		// $contactos->retrieve('1d346c89-2d23-87ad-b4ec-54d922478357');
		// global db;
		// $contact = BeanFactory::getBean('Contacts');
		// $all = $contact->get_full_list();
		// foreach ($all as $contact) {
		  // echo "{$contact->name} {$contact->account_name} \n <br>";
			$db = DBManagerFactory::getInstance();
			//funcion para capturar la configuracion para el consultor
			function get_configuracion($pais){
				//obtener las configuraciones
				global $db;
				$query_configuraciones=('SELECT * FROM cal_calculo where deleted="0"');
				$configuraciones = $db->query($query_configuraciones);
				//iterar sobre todas las configuraciones
				while($row = $db->fetchByAssoc($configuraciones)){
					//limpiar y y crear un array con la lista de los paises
					$cleanString = str_replace("^","", $row['paises']);
					$split = explode(",",$cleanString);
					//obtener solo la configuracion en correspondiente al contacto 
					if (in_array($pais, $split)) {
						$configuracion_valida = $row;
					}				    
				}
				return $configuracion_valida;
			};
			//obtener el periodo vigente  para usarlo en la creacion de 
			//la configuracion para el usuario en este lapso, en caso de que 
			//no exista 

                        
			$query_lapsos=("SELECT * FROM lapsos where activo = 1 ");
			$lapsos = $db->query($query_lapsos);
			while($row = $db->fetchByAssoc($lapsos)){
			    $id_lapso =  $row['id'];
                            $fecha_ini= date($row['fecha_inicio'].' 00:00:00');
                            $fecha_fin= date($row['fecha_final'].' 24:00:00');
                            
			}
			//query para saber si existe  un registro de "datos_calculo_aprovados"
			// y si no existe crearlo
			function usuario_mes_conf($id_lapso, $user_id){
				global $db ;
				$query_confi_aprob=("SELECT * FROM datos_calculo_aprobados where id_user_assigned = '".$user_id."' and id_lapso = '".$id_lapso."' ");
				$confi_aprob = $db->query($query_confi_aprob);
				while($row = $db->fetchByAssoc($confi_aprob)){
				    $configuracion_act = $row;
				}
				return $configuracion_act ;
			}
			
			//obtener la configuracion para el calculo
			$config_actual = get_configuracion($_GET[pais]);
			//la consulta previa que se usara para saber si se va crear o no la tabla mensual de captura de datos
			$master_configuracion_mensual = usuario_mes_conf($id_lapso, $_GET[id]);
			if (!$master_configuracion_mensual) {
				//aqui se creara la tabla 
                            $today=date("Y-m-d");
				$query_crear_config_mensual = "INSERT INTO `datos_calculo_aprobados` (`id`, `id_user_assigned`, `id_configuracion`, `id_lapso`, `opor_comercial_total`, `resp_a_requeri_total`, `potenci_inversio_total`, `fecha_creacion`, `delet`) VALUES ('0', '".$_GET[id]."', '".$config_actual[id]."', '".$id_lapso."', 0, 0, 0,'".$today."' , 0)";				
				$crear_config_mensual = $db->query($query_crear_config_mensual);
				//re-hace la consulta para ser usada en los datos que se van a guardar 
				$master_configuracion_mensual = usuario_mes_conf($id_lapso, $_GET[id]);
				// echo print_r($config_actual);				
			}
			// $query = "SELECT `users`.`id`,`users`.`user_name`, COUNT(DISTINCT `leads`.`id`) AS `num_leads`, COUNT(DISTINCT `contacts`.`id`) AS `num_contacts` FROM `users` LEFT OUTER JOIN `leads` ON ( `users`.`id` = `leads`.`assigned_user_id` ) LEFT OUTER JOIN `contacts` ON ( `users`.`id` = `contacts`.`assigned_user_id` ) GROUP BY `users`.`id` ORDER BY NULL";

			$hoy = date('2015-04-20 '.'00:00:00');
			$hoy1 = date('Y-m-d 00:00:01');
			// $hora = DateTime($hoy);
			// if ($hoy < $hoy1) {
			//     //                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      echo $hoy1;
			// }                        
                   //     $query_contacts = 'SELECT `cc`.* From `contacts` as c, `users` as u , `contacts_cstm` as cc  where `c`.`assigned_user_id`=`u`.`id`   and `u`.`id` = "'.$_GET[id].'" and `c`.`id`= `cc`.`id_c` and `cc`.`tipo_contac_c` IN (1,3) and `c`.`aprobado` = 0 and `c`.`date_entered` > "'.$fecha_ini.'" and `c`.`date_entered` <= "'.$fecha_fin.'"';
			//echo $query_contacts;
//                        $query_oportundidades ='SELECT `cc`.*,`B`.`tipo_oportunidad_c` from `users` as u , `opportunities` as cc,`opportunities_cstm`AS B
//                        where `cc`.`assigned_user_id` =`u`.`id` and `cc`.`id`= `B`.`id_c`
//                        and `u`.`id`  = "'.$_GET[id].'" 
//                        and `cc`.`aprobado` = 0 and `cc`.`date_entered` > "'.$fecha_ini.'" and `cc`.`date_entered` < "'.$fecha_fin.'"';
			//$query_cuentas = 'SELECT `cc`.* from `users` as u , `accounts` as cc where `cc`.`assigned_user_id` =`u`.`id` and `u`.`id`  = "'.$_GET[id].'"  and `cc`.`aprobado` = 0 ';
			$query_cuentas='SELECT DISTINCT `accounts`.`id`, `accounts`.`name`, `accounts`.`date_entered`, `accounts`.`date_modified`, `accounts`.`modified_user_id`,`B`.`nombre_comercial_c`,`B`.`tipo_servicio_c` FROM `accounts` INNER JOIN `documents_accounts` 
                        ON ( `accounts`.`id` = `documents_accounts`.`account_id` )
                        INNER JOIN `accounts_cstm`AS `B`
                        ON (`accounts`.`id`= `B`.`id_c`)                                                                                                                                                                                                                                                                                                                                      
                        WHERE ((`documents_accounts`.`document_id`) 
                        IN (SELECT `documents`.`id` FROM `documents`) 
                        AND `accounts`.`assigned_user_id` = "'.$_GET[id].'" AND `accounts`.`aprobado` = 0 and `documents_accounts`.`deleted` = 0 and `accounts`.`date_entered` > "'.$fecha_ini.'" and `accounts`.`date_entered` <= "'.$fecha_fin.'")';
			//inversion   tipo_contac_c 3                    
			$contacts_inversion= 'SELECT DISTINCT `contacts`.`id`, `contacts`.`first_name`, `contacts`.`date_entered`, `contacts`.`date_modified`, `contacts`.`modified_user_id`,`B`.`nombre_emp_c` 
						FROM `contacts` INNER JOIN `opportunities_contacts` ON ( `contacts`.`id` = `opportunities_contacts`.`contact_id` ) INNER JOIN `contacts_cstm`AS `B` ON (`contacts`.`id`= `B`.`id_c`)
						WHERE ((`opportunities_contacts`.`opportunity_id`) IN (SELECT `opportunities`.`id` FROM `opportunities`) AND `contacts`.`assigned_user_id` = "'.$_GET[id].'" 
						AND `contacts`.`aprobado` = 0 and `contacts`.`date_entered` > "'.$fecha_ini.'" and `contacts`.`date_entered` <= "'.$fecha_fin.'" and `opportunities_contacts`.`deleted` = 0 and `B`.`tipo_contac_c` = 3)';   


			//comercio tipo_contac_c 1
			$contacts_comerciales= 'SELECT DISTINCT `contacts`.`id`, `contacts`.`first_name`, `contacts`.`date_entered`, `contacts`.`date_modified`, `contacts`.`modified_user_id`,`B`.`nombre_emp_c` 
						FROM `contacts` INNER JOIN `opportunities_contacts` ON ( `contacts`.`id` = `opportunities_contacts`.`contact_id` ) INNER JOIN `contacts_cstm`AS `B` ON (`contacts`.`id`= `B`.`id_c`)
						WHERE ((`opportunities_contacts`.`opportunity_id`) IN (SELECT `opportunities`.`id` FROM `opportunities`) AND `contacts`.`assigned_user_id` = "'.$_GET[id].'" 
						AND `contacts`.`aprobado` = 0 and `contacts`.`date_entered` > "'.$fecha_ini.'" and `contacts`.`date_entered` <= "'.$fecha_fin.'" and `opportunities_contacts`.`deleted` =0 and `B`.`tipo_contac_c` = 1)';   

// echo $contacts_inversion  ;
//$cuentas2 = $db->query($test);//echo $query_cuentas;
//while($row = $db->fetchByAssoc($cuentas2)){
//        echo 's';
//
//};
// // $res = $db->query($query);                                                                                                                                
			// $tabla = $db->query($query);
//echo $query_cuentas;

			$cont = $db->query($contacts_inversion);
			$oportunidades = $db->query($contacts_comerciales);
			$cuentas = $db->query($query_cuentas);


                        
                        

			// echo var_dump($cuentas1);
			// $mirar =$cont = $db->query($query_contacts);
			// $row = $db->fetchByAssoc($res);
			// echo $row;
			
			// echo print_r($row);
			// echo print_r($row);
			// while($row = $db->fetchByAssoc($mirar)){
			// 	// echo $row[id]." ".$row[user_name]." ".$row[num_leads]." ".$row[num_contacts]." \n <br>";
			// 	echo $row[id];
			// 	echo '<br>';
			// };
		// // require_once('include/SugarQuery/SugarQuery.php');
		// $query = new SugarQuery();
		// $query->select(array('id','name'));
		// $query->from(BeanFactory::newBean('Contacts'));
		// $query->where()->equals('id','1d346c89-2d23-87ad-b4ec-54d922478357');
		// $results = $query->execute(); 
		// echo $results;
		// };
	 // parent::display();
		$term = 20;
		$max_sentence = 100;
		$sentency_severity = round(($term / $max_sentence) * 100);
		echo '<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>';
		echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">';
		echo '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>' ;
		// echo '<script src="include/javascript/raphael.2.1.0.min.js"></script>';
		echo '<script src="modules/q1w2e_probando/cajas.js"></script>';

		
		echo '<link rel="stylesheet" href="modules/q1w2e_probando/style.css">';
                echo '<link rel="stylesheet" href="modules/q1w2e_probando/views/salida_css.css">';
		// echo '<script src="include/javascript/justgage.1.0.1.js"></script>';
		// echo '<img src="http://chart.apis.google.com/chart?chs=320x160&amp;chl='.$term.'%&amp;cht=gm&amp;chco=0000000,009900|FF0000&amp;chxt=x,y&chxl=y|0:||0.5:||1:|failure|midle|success&amp;chd=t:'.$sentency_severity.'" width="320" height="160" alt="Sentence Severity" />';
		echo "<br>";
		// echo '<img src="http://chart.apis.google.com/chart?chs=520x520&amp;chld=CA-BC|CN|IT|GR|MX&amp;chdl=Vancouver|Beijing|Torino|Athens|El+Salvador&amp;cht=map:fixed=-60,0,80,-35&amp;chco=B3BCC0|5781AE|FF0000|FFC726|885E80|518274&amp;chm=fConsultor+Ronald,000000,0,0,10|f2008+Summer,000000,0,1,10|f2008+Winter,000000,0,2,10,1,:-5:10|f2004+Summer,000000,0,3,10|f2004+Summer,000000,0,4,10&amp;chma=0,110,0,0" width="620" height=620" alt="Sentence Severity" />';
		// echo '    <div id="gauge" class="200x160px"></div>';
	// 	echo '    <script>
 //    var g = new JustGage({
 //    id: "gauge",
 //    value: getRandomInt(0, 100),
 //    min: 0,
 //    max: 100,
 //    title: "Visitors",
 //    label: "%"
 //    });
	// setInterval(function() {
 //          g.refresh(getRandomInt(0, 100));
 //        }, 2500);
 //    </script>';

		// $user=BeanFactory::getBean('Users','a4a4ab78-0df9-f5bc-a815-54f0c896885e');
		// echo  $primary_email=$user->email1;
		// echo "string";
		// $user=BeanFactory::getBean('Users', 'c080513d-9ec7-408d-8da3-54d8d9453a37'); 
		// echo $primary_email=$user->emailAddress->getPrimaryAddress($user);
		// $user=BeanFactory::getBean('Users', 'c080513d-9ec7-408d-8da3-54d8d9453a37'); 
		// echo $primary_email=$user->enlace_sv_c;

     $tipo_contacto = array(3=>'Inversión',1=>'Comercio');
     $tipo_oportunidad=$GLOBALS['app_list_strings']['tipo_servicio_list'];
     echo '<div class="container">
	<div class="row">

        <div class="dual-list list-left col-md-5">
            <div class="well text-right">
                <div class="row">
                    <div class="col-md-10">
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-search"></span>
                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="btn-group">
                            <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>
                        </div>
                    </div>
                    
                </div>
                <br>
                <ul class="list-group" id="listas">';
                while($row = $db->fetchByAssoc($cont)){
                        echo '<li class="list-group-item" id="'.$row[id].'" data-tipo="contacts_inversion"><spam id="nombre_empc"><div > <spam class="texto_contactos">Potencial Inversionista -</spam> <spam style="font-size: 1.5em;">'.$row[nombre_emp_c].'</spam></spam ></div><a href="index.php?module=Contacts&action=DetailView&record='.$row[id].' "class="btn btn-success cblanca ir_ya" >Ver <span class="glyphicon glyphicon-exclamation-sign"></span></a> </li>';
				
                };
				while($row = $db->fetchByAssoc($oportunidades)){
                	echo '<li data-toggle="popover" data-trigger="hover" data-placement="left" title="Oportunidades Comerciales" class="list-group-item" id="'.$row[id].'" data-tipo="contacts_comerciales"><spam id="nombre_empc"><div><spam class="texto_oportunidad">Oportunidades Comerciales - </spam><spam style="font-size: 1.5em;">'.$row[nombre_emp_c].'</spam></div><a href="index.php?module=Contacts&action=DetailView&record='.$row[id].' "class="btn btn-success cblanca ir_ya" >Ver <span class="glyphicon glyphicon-exclamation-sign"></span></a> </li>';
				};
                while($row = $db->fetchByAssoc($cuentas)){
                	echo '<li class="list-group-item" id="'.$row[id].'" data-tipo="accounts"><spam id="nombre_empc"><div><spam style="font-size: 1.5em;">Respuesta a requerimiento - </spam>'.$row[nombre_comercial_c].'</spam ><spam class="texto_cuentas"> - Cuentas -'.$tipo_oportunidad [$row[tipo_servicio_c]].'</spam></div><a href="index.php?module=Accounts&action=DetailView&record='.$row[id].' "class="btn btn-success cblanca ir_ya" >Ver <span class="glyphicon glyphicon-exclamation-sign"></span></a> </li>';	
                        
                };    
               echo '</ul>
            </div>
        </div>

        <div class="list-arrows col-md-1 text-center">
            <button class="btn btn-default btn-sm move-left">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </button>

            <button class="btn btn-default btn-sm move-right">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </button>
        </div>

        <div class="dual-list list-right col-md-5">
            <div class="well text-right">
                <div class="row">
                    <div class="col-md-2">
                        <div class="btn-group">
                            <a class="btn btn-default selector" title="select all"><i class="glyphicon glyphicon-unchecked"></i></a>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div class="input-group">
                            <input type="text" name="SearchDualList" class="form-control" placeholder="search" />
                            <span class="input-group-addon glyphicon glyphicon-search"></span>
                        </div>
                    </div>
                </div>
                <br>
                <ul class="list-group">
                    
                </ul>
               <!-- <a href="index.php?module=q1w2e_probando&action=json" class="btn btn-default" id="salvar">ir</a> -->
               
                <a data-toggle="modal" href="#myModal"class="btn btn-success " style="color: white;">Validar <spam class="glyphicon glyphicon-floppy-disk"></spam></a>
            </div>
        </div>

	</div>
</div>';


include('modules/q1w2e_probando/Email.php');
$mensaje = new Email();
// $mensaje->sendMessage($objetos);
 ?>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Esta seguro que desea validar estos registros?</h4>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                <button class="btn btn-success" id="salvar" data-loading-text="Enviando...">Validar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
$(function () {
 $('[data-toggle="popover"]').popover(
         {
            trigger: 'hover' 
         });
});    
</script>    
<?php
 echo '<script>
function myFunction() {
    
}

$(".ir_ya").click(function(e){
	e.preventDefault();
	window.open(""+$(this).attr("href")+"", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=200, left=500, width=600, height=400");
});

$("#salvar2").click(function(e){
e.preventDefault();
    alert("asd");
});



var master_id ;
var master_tipo;
var master = "'.$master_configuracion_mensual[id].'";
var id_usuario ="'.$_GET[id].'";
$("#salvar").click(function(e){
	e.preventDefault;
        var $btn = $(this).button("loading");
	var request = $.ajax({
	  url: "index.php?entryPoint=detalle_salvado",
	  method: "POST",
	  data: { ids : master_id.toString(),
	  			tipos_d: master_tipo.toString(),
	  			master_id_id : master,
				usuario: id_usuario,
	  			 },
	//   dataType: "html"
	});
	 
	request.done(function( msg ) {
	  console.log(msg);
          window.location.href = "index.php?module=q1w2e_probando&action=json&master="+master;
          $btn.button("reset")
          
	});
	 
	request.fail(function( jqXHR, textStatus ) {
	  console.log( "Request failed: " + textStatus );
	});
});







</script>';
// echo "<script>
// 	$('#listas').find('li').each(function() {
   
//     console.log( $(this).attr('data-tipo'));
//     console.log( $(this).attr('id'));
//     console.log( $(this).attr('class'));
// });
	
// </script>";
// echo '<script>
// 	$.ajax({
// 		method: "GET",
// 		url: "http://localhost/lol/test/index.php?module=q1w2e_probando&action=json",
// 		data:{ name: "John", location: "Boston" }
// 	})
// 	.done(function( msg ) {
// 		console.log( "Data Saved: " + msg );
// 	});

// </script>';
// echo '
// <script>
// function invoke() {
//     alert("Button clicked!");
//     var callback = {
//         success: function(o) {
//             document.getElementById("div_info").innerHTML = 
//                 o.responseText;
//         }
//     }
    
//     var connectionObject = YAHOO.util.Connect.asyncRequest ("GET", "http://localhost/lol/test/json.php", callback);
// }
// </script>  

// <input type="button" id="boton" name="button" value="Execute PHP" onclick="invoke();">

// <div id="div_info">
// <b>this will change...</b>
// </div>
// ';  


// $idiomas = $GLOBALS['app_list_strings']['idioma_list1'];
// include('include/languaje/en_us.lang.php');
// $idiomas = $app_list_strings['countries_dom'];
// $idiomas = $GLOBALS['app_list_strings']['countries_dom'];
// // echo print_r($idiomas);

// function js_str($s)
// {
//     return '"' . addcslashes($s, "\0..\37\"\\") . '"';
// }

// function js_array($array)
// {
//     $temp = array_map('js_str', $array);
//     return '[' . implode(',', $temp) . ']';
// }

// echo 'var cities = ', js_array($idiomas), ';';

	}

}

 ?>	