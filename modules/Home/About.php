<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


//NOTE: Under the License referenced above, you are required to leave in all copyright statements in both
//the code and end-user application.

global $sugar_config, $mod_strings;
?>
<link rel="stylesheet" href="include/css/bootstrap.css">
<link rel="stylesheet" href="include/css/acercade.css">
  <div class="container">
  	<div class="row">
  		<h1 id="que-son-las-consejerias-economicas-comerciales-y-de-turismo">
  		 ¿Qué son las Consejerías Económicas, Comerciales y de Turismo?
  		</h1>

  		<div id="acerca_div" class="col-lg-12">
  			<ul class="nav navbar-nav">
  			        <li class="dropdown">
  			          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu Acerca de <span class="glyphicon glyphicon-user pull-right"></span></a>
  			          <ul class="dropdown-menu">
  			            <li> <a href="#chapter-1">Competencias de trabajo  <span class="glyphicon glyphicon-cog pull-right"></span></a></li>
  			            <li class="divider"></li>
  			            <li> <a href="#chapter-2">Servicios que ofrecen  <span class="glyphicon glyphicon-stats pull-right"></span></a></li>
  			           <!-- <li class="divider"></li>
  			            <li><a href="#">Messages <span class="badge pull-right"> 42 </span></a></li>
  			            <li class="divider"></li>
  			            <li><a href="#">Favourites Snippets <span class="glyphicon glyphicon-heart pull-right"></span></a></li>
  			            <li class="divider"></li>
  			            <li><a href="#">Sign Out <span class="glyphicon glyphicon-log-out pull-right"></span></a></li> -->
  			          </ul>
  			        </li>
  			      </ul>
  		</div>
  		
  		<p id="parrafo_imagen">
  		 <img id='imagen_acerca' class="img-thumbnail" alt="Salvador" src="include/images/salvador-tormenta.jpg" title="Salvador"/>
  		</p>
  		<h4 id="competencias-de-trabajo-de-las-cect">
  		 Competencias de trabajo de las CECT
  		 <hr>
  		 <a id="chapter-1">
  		 </a>
  		</h4>
		<div class="error-notice">
          <div class="oaerror danger">
            <strong><span class="glyphicon glyphicon-play"></span></strong> Apoyar la política de atracción de inversión extranjera directa (IED).
          </div>
          <div class="oaerror warning">
            <strong><span class="glyphicon glyphicon-play"></span></strong>  Apoyar la promoción de exportaciones de productos y servicios  salvadoreños.
          </div>
          <div class="oaerror info">
            <strong><span class="glyphicon glyphicon-play"></span></strong>  Apoyo la Estrategia Nacional de Promoción del Turismo.
          </div>
        </div>

  		
  		<h4 id="servicios-que-ofrecen-al-sector-empresarial">
  		 Servicios que ofrecen al sector empresarial:
  		 <hr>
  		 <a id="chapter-2">
  		 </a>
  		</h4>
  		<ul>
  		 <li>
  		  Identificar potenciales importadores/exportadores de sectores/productos salvadoreños.
  		 </li>
  		 <li>
  		  Realizar exploraciones de mercado de sectores/productos de interés del exportador.
  		 </li>
  		 <li>
  		  Participar en ferias comerciales.
  		 </li>
  		 <li>
  		  Promover la oferta exportable de El Salvador.
  		 </li>
  		 <li>
  		  Organizar ruedas de negocios y encuentros empresariales /inversionistas en el país sede.
  		 </li>
  		 <li>
  		  Apoyar en la visita al mercado destino de exportadores.
  		 </li>
  		 <li>
  		  Identificar las tendencias y oportunidades para los sectores de interés de El Salvador en el mercado asignado.
  		 </li>
  		 <li>
  		  Proporcionar vínculos con empresas y otras instituciones interesadas en hacer negocios, o realizar inversiones con El Salvador.
  		 </li>
  		 <li>
  		  Proporcionar acceso a información comercial del mercado asignado.
  		 </li>
  		</ul>
  	</div>
  </div>


<script src="include/javascript/jquery-1.11.2.js"></script>
<script src="include/javascript/bootstrap.js"></script>