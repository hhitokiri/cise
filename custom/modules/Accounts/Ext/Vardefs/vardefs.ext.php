<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-03-18 08:49:41
$dictionary['Account']['fields']['detalle_bienes_c']['labelValue']='Detalle la Información que requiere validar o completar ';

 

 // created: 2015-03-17 09:08:58
$dictionary['Account']['fields']['serv_sociales_salud_c']['labelValue']='Servicios sociales y de salud';

 

 // created: 2015-03-10 12:41:19
$dictionary['Account']['fields']['empaq_empof_c']['labelValue']='Empaque adecuado';

 

 // created: 2015-03-13 09:47:50
$dictionary['Account']['fields']['temporal_h_c']['labelValue']='Temporales Hombres';

 

 // created: 2015-03-10 12:18:21
$dictionary['Account']['fields']['rango1_empof_c']['labelValue']='Rango de precios al consumidor final (Max)';

 

 // created: 2015-03-10 12:09:50
$dictionary['Account']['fields']['capc_empof_c']['labelValue']='Capacidad de Exportación ';

 

 // created: 2015-03-09 15:27:44
$dictionary['Account']['fields']['tel2_c']['labelValue']='Tel.(2)';

 

 // created: 2015-03-17 09:07:24
$dictionary['Account']['fields']['serv_construccion_c']['labelValue']='Servicios de construcción y servicios conexos';

 

 // created: 2015-04-22 08:55:22
$dictionary['Account']['fields']['otra_oportunidad_c']['labelValue']='Otra Oportunidad (Especifique)';

 

 // created: 2015-03-17 09:06:08
$dictionary['Account']['fields']['serv_financieros_c']['labelValue']='Servicios Financieros';

 

 // created: 2015-03-17 09:29:49
$dictionary['Account']['fields']['serv_prestados_c']['labelValue']='Servicios prestados a las empresas y servicios profesionales';

 

 // created: 2015-03-17 09:05:26
$dictionary['Account']['fields']['serv_recreativos_c']['labelValue']='Servicios recreativos, culturales y deportivos';

 

 // created: 2015-03-10 12:45:43
$dictionary['Account']['fields']['socioe_empof_c']['labelValue']='Favor detalle el nivel socioeconómico al que va dirigido su producto o servicio';

 

 // created: 2015-03-09 15:56:56
$dictionary['Account']['fields']['sector_economico_c']['labelValue']='Sector económico al que pertenece ';

 

 // created: 2015-03-10 11:54:29
$dictionary['Account']['fields']['nombre_pervia_c']['labelValue']='Nombre1per';

 

 // created: 2015-03-10 11:51:04
$dictionary['Account']['fields']['tel_conprin_c']['labelValue']='Tel.';

 

 // created: 2015-03-17 09:03:15
$dictionary['Account']['fields']['serv_ambiente_c']['labelValue']='Servicios relacionados con el medio ambiente';

 

 // created: 2015-03-11 14:29:09
$dictionary['Account']['fields']['name']['required']=true;
$dictionary['Account']['fields']['name']['comments']='Name of the Company';
$dictionary['Account']['fields']['name']['merge_filter']='disabled';

 

 // created: 2015-03-10 12:43:05
$dictionary['Account']['fields']['certi_empof_c']['labelValue']='Certificaciones';

 

 // created: 2015-03-10 11:49:52
$dictionary['Account']['fields']['cargo_conprin_c']['labelValue']='Cargo';

 

 // created: 2015-03-13 09:47:12
$dictionary['Account']['fields']['fijos_m_c']['labelValue']='Fijos Mujeres';

 

 // created: 2015-03-10 11:56:48
$dictionary['Account']['fields']['cargo1_pervia_c']['labelValue']='Cargo2 person';

 

 // created: 2015-03-10 11:53:15
$dictionary['Account']['fields']['email1_conprin_c']['labelValue']='Email';

 

 // created: 2015-03-09 16:23:21
$dictionary['Account']['fields']['acuerdos_c']['labelValue']='Acuerdos que le interesan: (Seleccionar)';

 

 // created: 2015-03-10 11:55:09
$dictionary['Account']['fields']['nomber1_pervia_c']['labelValue']='Nombre2 person';

 

 // created: 2015-03-09 15:31:42
$dictionary['Account']['fields']['fecha_fundacion_c']['labelValue']='Fecha de Fundación ';

 

 // created: 2015-03-19 11:02:39
$dictionary['Account']['fields']['serv_comunicaciones_c']['labelValue']='Servicios de comunicaciones';

 

 // created: 2015-03-18 08:50:12
$dictionary['Account']['fields']['especificar_otrosector_c']['labelValue']='Otros productos no incluidos en esas categorías (especificar) ';

 

 // created: 2015-03-17 09:16:05
$dictionary['Account']['fields']['serv_energia_c']['labelValue']='Servicio de energía ';

 

 // created: 2015-03-13 10:11:08
$dictionary['Account']['fields']['s_ofrece_serv_c']['labelValue']='Describa clara y detalladamente las lineas de servicios que su empresa ofrece o de las cuales requiere información ';

 

// created: 2015-03-12 12:35:05
$dictionary["Account"]["fields"]["accounts_idc_informaciondecomercio_1"] = array (
  'name' => 'accounts_idc_informaciondecomercio_1',
  'type' => 'link',
  'relationship' => 'accounts_idc_informaciondecomercio_1',
  'source' => 'non-db',
  'module' => 'idc_InformacionDeComercio',
  'bean_name' => 'idc_InformacionDeComercio',
  'vname' => 'LBL_ACCOUNTS_IDC_INFORMACIONDECOMERCIO_1_FROM_IDC_INFORMACIONDECOMERCIO_TITLE',
);


 // created: 2015-03-17 09:18:28
$dictionary['Account']['fields']['serv_distrib_c']['labelValue']='Servicio de distribución ';

 

 // created: 2015-03-10 12:18:39
$dictionary['Account']['fields']['rango_empof_c']['labelValue']='Rango de precios al consumidor final (Min)';

 

 // created: 2015-03-10 12:07:14
$dictionary['Account']['fields']['nomprod_empof_c']['labelValue']='Nombre del producto';

 

 // created: 2015-03-10 12:16:34
$dictionary['Account']['fields']['perio_empof_c']['labelValue']='Periodicidad';

 

 // created: 2015-03-18 08:51:08
$dictionary['Account']['fields']['otros_acuerdos_c']['labelValue']='Otros Acuerdos (especificar)';

 

 // created: 2015-03-13 09:48:29
$dictionary['Account']['fields']['temporal_m_c']['labelValue']='Temporales Mujeres';

 

 // created: 2015-03-09 16:07:58
$dictionary['Account']['fields']['perfil_clientepot_c']['labelValue']='Cual es su perfil a su cliente potencial? Describa detalladamente.';

 

 // created: 2015-03-10 12:12:47
$dictionary['Account']['fields']['uni_empof_c']['labelValue']='Unidad';

 

 // created: 2015-03-12 11:03:14
$dictionary['Account']['fields']['ngust_empof_c']['labelValue']='Enliste las empresas con las que no le gustaria reunirse, pues ya posee un contacto comercial con el cual esta trabajando actualmente o porque no le interesa';

 

 // created: 2015-03-13 12:32:07
$dictionary['Account']['fields']['tipo_servicio_c']['labelValue']='Tipo';

 

 // created: 2015-03-13 10:23:42
$dictionary['Account']['fields']['servicios_bienes_c']['labelValue']='Seleccione que servicio desea solicitar';

 

 // created: 2015-03-12 11:08:13
$dictionary['Account']['fields']['suge_empof_c']['labelValue']='Si tiene sugerencias de las empresas con las que le gustaría reunirse, enliste los nombres';

 

 // created: 2015-03-10 12:00:21
$dictionary['Account']['fields']['idioma1_pervia_c']['labelValue']='Idioma2 person';

 

 // created: 2015-03-09 14:42:55
$dictionary['Account']['fields']['consejeria_c']['labelValue']='Seleccione de que consejería requiere apoyo ';

 

 // created: 2015-03-10 12:02:31
$dictionary['Account']['fields']['ciudad_pervia_c']['labelValue']='Ciudad a Participar';

 

 // created: 2015-03-10 11:48:15
$dictionary['Account']['fields']['nombre_conprin_c']['labelValue']='Nombre';

 

 // created: 2015-03-10 11:57:15
$dictionary['Account']['fields']['email_pervia_c']['labelValue']='Email person';

 

 // created: 2015-03-09 15:18:29
$dictionary['Account']['fields']['razon_social_c']['labelValue']='Razón Social';

 

 // created: 2015-03-10 11:57:57
$dictionary['Account']['fields']['email1_pervia_c']['labelValue']='Email2 person';

 

 // created: 2015-03-10 11:55:57
$dictionary['Account']['fields']['cargo_pervia_c']['labelValue']='Cargo person';

 

 // created: 2015-03-18 11:53:41
$dictionary['Account']['fields']['pararan_empof_c']['labelValue']='Partida Arancelaria1';

 

 // created: 2015-03-09 15:26:46
$dictionary['Account']['fields']['tel1_c']['labelValue']='Tel.(1)';

 

 // created: 2015-03-13 09:46:36
$dictionary['Account']['fields']['fijos_h_c']['labelValue']='Fijos Hombres';

 

 // created: 2015-03-09 15:26:04
$dictionary['Account']['fields']['nit_c']['labelValue']='NIT';

 

 // created: 2015-03-17 09:20:30
$dictionary['Account']['fields']['serv_turismo_c']['labelValue']='Servicios de turismo y viajes';

 

 // created: 2015-03-10 11:51:45
$dictionary['Account']['fields']['tel1_conprin_c']['labelValue']='Tel.';

 

 // created: 2015-03-13 11:13:35
$dictionary['Account']['fields']['acuerdos_serv_c']['labelValue']='Acuerdos que le interesan (Seleccionar - S) ';

 

 // created: 2015-03-10 11:50:28
$dictionary['Account']['fields']['cargo1_conprin_c']['labelValue']='Cargo';

 

 // created: 2015-03-09 15:28:47
$dictionary['Account']['fields']['fax_c']['labelValue']='Fax';

 

 // created: 2015-03-10 11:59:18
$dictionary['Account']['fields']['idioma_pervia_c']['labelValue']='Idioma person';

 

 // created: 2015-03-10 12:42:22
$dictionary['Account']['fields']['manuf_empof_c']['labelValue']='Buenas practicas de manufactura';

 

 // created: 2015-03-10 12:43:49
$dictionary['Account']['fields']['otro_empof_c']['labelValue']='Otros explique';

 

 // created: 2015-03-10 11:49:05
$dictionary['Account']['fields']['nombre1_conprin_c']['labelValue']='Nombre';

 

 // created: 2015-03-09 15:33:51
$dictionary['Account']['fields']['nu_trabajadores_c']['labelValue']='N° de Trabajadores';

 

 // created: 2015-03-12 15:01:36
$dictionary['Account']['fields']['reg_empof_c']['labelValue']='Registro Sanitario ';

 

 // created: 2015-03-18 08:50:43
$dictionary['Account']['fields']['persona_contacto_c']['labelValue']='Escriba el nombre y el cargo de la persona con la que usualmente realiza sus contactos';

 

 // created: 2015-03-10 11:52:40
$dictionary['Account']['fields']['email_conprin_c']['labelValue']='Email';

 

 // created: 2015-03-09 15:20:12
$dictionary['Account']['fields']['direccion_fisica_c']['labelValue']='Dirección Física de Oficina Principal';

 

 // created: 2015-03-09 15:30:05
$dictionary['Account']['fields']['email_c']['labelValue']='E-mail';

 

 // created: 2015-03-09 15:19:06
$dictionary['Account']['fields']['nombre_comercial_c']['labelValue']='Nombre Comercial';

 

 // created: 2015-03-17 09:19:31
$dictionary['Account']['fields']['serv_transporte_c']['labelValue']='Servicio de transporte y logística ';

 

 // created: 2015-03-18 08:49:09
$dictionary['Account']['fields']['expectativas_c']['labelValue']='Que expectativas tiene con este requerimiento de apoyo? Describa detalladamente.';

 

 // created: 2015-03-17 09:17:38
$dictionary['Account']['fields']['serv_ense_c']['labelValue']='Servico de enseñanza';

 

 // created: 2015-03-09 15:30:48
$dictionary['Account']['fields']['sitio_web_c']['labelValue']='Sitio Web';

 

 // created: 2015-03-09 14:58:46
$dictionary['Account']['fields']['otro_servicio_bienes_c']['labelValue']='Otro Servicio';

 

 // created: 2015-03-10 12:01:53
$dictionary['Account']['fields']['fecha_pervia_c']['labelValue']='fecha pervia';

 
?>