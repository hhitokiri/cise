<?php
// created: 2015-04-23 11:05:40
$mod_strings = array (
  'LBL_MONTO_VENTASNAC' => 'Monto de ventas nacionales (USD)',
  'LBL_MONTO_EXPOR' => 'Monto de exportaciones (USD)',
  'LBL_MONTO_IMPORT' => 'Monto de importaciones (USD)',
  'LBL_DESTINO_EXPOR' => 'Destino de las exportaciones',
  'LBL_ORIGEN_IMPORT' => 'Origen de las importaciones',
  'LBL_EXP_EVENTOS' => 'Ha tenido experiencia en eventos comerciales internacionales (Ferias, Ruedas, Misiones)?',
  'LBL_CONQ_ENTIDAD' => 'Con que entidad?',
  'LBL_CONCEPTO_ANIO' => 'Año',
  'LBL_EDITVIEW_PANEL1' => 'Información de comercio internacional (Últimos 3 años)',
  'LBL_YEAR' => 'Año',
  'LBL_HOMEPAGE_TITLE' => 'Mi Información de comercio',
);