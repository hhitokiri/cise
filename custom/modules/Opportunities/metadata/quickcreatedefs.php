<?php
$viewdefs ['Opportunities'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{$PROBABILITY_SCRIPT}',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL6' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL5' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL2' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'include/javascript/hiko.js',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_editview_panel6' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'tipo_oportunidad_c',
            'studio' => 'visible',
            'label' => 'LBL_TIPO_OPORTUNIDAD',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'sector_opor_servicios_c',
            'studio' => 'visible',
            'label' => 'LBL_SECTOR_OPOR_SERVICIOS',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'caracteristicas_servicio_c',
            'studio' => 'visible',
            'label' => 'LBL_CARACTERISTICAS_SERVICIO',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel5' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'nombre1_c',
            'label' => 'LBL_NOMBRE1',
          ),
          1 => '',
        ),
      ),
      'lbl_editview_panel2' => 
      array (
        0 => 
        array (
          0 => '',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
              'required' => true,
            ),
          ),
          1 => 
          array (
            'name' => 'account_name',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'caract_c',
            'studio' => 'visible',
            'label' => 'LBL_CARACT',
          ),
          1 => 
          array (
            'name' => 'espe_tec_c',
            'studio' => 'visible',
            'label' => 'LBL_ESPE_TEC',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'uni_med_c',
            'label' => 'LBL_UNI_MED',
          ),
          1 => 
          array (
            'name' => 'volu_des_c',
            'label' => 'LBL_VOLU_DES',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'for_pag_c',
            'label' => 'LBL_FOR_PAG',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'req_espc_c',
            'studio' => 'visible',
            'label' => 'LBL_REQ_ESPC',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'sec_confec_c',
            'label' => 'LBL_SEC_CONFEC',
          ),
          1 => '',
        ),
        7 => 
        array (
          0 => 
          array (
            'name' => 'fec_req_c',
            'label' => 'LBL_FEC_REQ',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'assigned_user_name',
          ),
        ),
      ),
    ),
  ),
);
?>
