<?php
// created: 2015-03-11 12:12:53
$mod_strings = array (
  'LBL_CARGO_V' => 'Cargo',
  'LBL_ALC_GEO' => 'Alcance Geografico',
  'LBL_TIP_PRO' => 'Tipo de Productos',
  'LBL_A123_REFERENCIASCOMERCIALES_CONTACTS_1_FROM_CONTACTS_TITLE' => 'Contactos',
  'LBL_PAISC' => 'Pais',
  'LBL_NOMBRE_CONTACTV' => 'Nombre de Contacto',
  'LBL_TEL_V' => 'Telefono',
  'LBL_EMAIL_V' => 'Email',
  'LBL_VEN_ANU' => 'Ventas Anuales',
  'LBL_NU_BODE' => 'Numero de Bodegas y sus dimensiones',
  'LBL_ALC_GEO1' => 'Alcance geográfico en cuanto a distribucion',
  'LBL_HOMEPAGE_TITLE' => 'Mi Referencias Comerciales (Empresas a las que vende)',
  'LBL_QUICKCREATE_PANEL1' => 'Referencias Comerciales (Empresas a las que vende)',
  'LBL_QUICKCREATE_PANEL2' => 'Nuevo Panel 2',
  'LBL_COMENT_V' => 'Comentarios adicionales a considerar',
  'LBL_NOMBRE_EMPRESA' => 'Nombre de la empresa',
  'LBL_TIPO_DE_PRODUCTO' => 'Tipo de producto que distribuye ',
);