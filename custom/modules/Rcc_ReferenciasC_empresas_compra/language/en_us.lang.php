<?php
// created: 2015-04-23 10:57:56
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Trade References (EmpC)',
  'LNK_LIST' => 'View Trade References (EmpC)',
  'LNK_IMPORT_RCC_REFERENCIASC_EMPRESAS_COMPRA' => 'Importar Trade References (EmpC)',
  'MSG_SHOW_DUPLICATES' => 'The account record you are about to create might be a duplicate of an account record that already exists. Account records containing similar names are listed below.Click Save to continue creating this new account, or click Cancel to return to the module without creating the account.',
  'LBL_LIST_FORM_TITLE' => 'Trade References (EmpC) List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Trade References (EmpC)',
);