<?php 

if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 

require_once('include/MVC/View/views/view.detail.php');
/**
* 
*/
class ViewJsonView extends ViewDetail{
	
	function ViewJsonView(){
		parent::ViewDetail();
	}
	
	function display() {
            
                $db = DBManagerFactory::getInstance();
                $master = $_GET[master];
                
                $query='SELECT * FROM datos_calculo_aprobados WHERE id= "'.$master.'"';
                
                $datos= $db->query($query);
                while ($row = $db->fetchByAssoc($datos)){
                    $master_datos=$row;
                } 
                
                $query_config='SELECT * FROM cal_calculo WHERE id= "'.$master_datos[id_configuracion].'"';
                $datos_config= $db->query($query_config);
                while ($row = $db->fetchByAssoc($datos_config)){
                $datos_c=$row;    
                
               }
               $respuesta_a_requerimiento=$datos_c[respuesta_a_requerimiento];
               $respuesta_a_requerimiento_n= $datos_c[respuesta_a_requerimiento_n];
               $potenciales_inversionistas= $datos_c[potenciales_inversionistas];
               $potenciales_inversionistas_n= $datos_c[potenciales_inversionistas_n];
               $oportunidades_comerciales= $datos_c[oportunidades_comerciales];
               $oportunidades_comerciales_n= $datos_c[oportunidades_comerciales_n];

               $resp_a_requeri_total=$master_datos[resp_a_requeri_total];
               $potenci_inversio_total=$master_datos[potenci_inversio_total];
               $opor_comercial_total=$master_datos[opor_comercial_total];
               
               //Calculo Variable 
               
               //
               echo 'cuentas<br>';
               echo $respuesta_a_requerimiento_n. '% <br>';
               echo $resp_a_requeri_total. '<br>';
               echo $respuesta_a_requerimiento.'<br>';
               //Accounts
               echo $Porciento_cuenta=($respuesta_a_requerimiento_n * $resp_a_requeri_total)/ $respuesta_a_requerimiento;
               echo '<br>';
               echo 'Contactos<br>';
               echo $potenciales_inversionistas_n. '% <br>';
               echo $potenci_inversio_total. '<br>';
               echo $potenciales_inversionistas.'<br>';
               //Contacts
               echo $Porciento_contactos=($potenciales_inversionistas_n * $potenci_inversio_total )/ $potenciales_inversionistas;
               //opportunities
               echo '<br>';
               echo 'Oportunidades<br>';
               echo $oportunidades_comerciales_n. '% <br>';
               echo $opor_comercial_total. '<br>';
               echo $oportunidades_comerciales.'<br>';
               echo $Porciento_oportunidades=($oportunidades_comerciales_n * $opor_comercial_total)/ $oportunidades_comerciales;
               
               $total=$Porciento_cuenta+$Porciento_contactos+$Porciento_oportunidades;
               
               // echo print_r($datos_c);
                //echo $master;
		echo '<script src="include/javascript/raphael.2.1.0.min.js"></script>';
		echo '<script src="include/javascript/justgage.1.0.1.js"></script>';
		echo '<link rel="stylesheet" href="modules/q1w2e_probando/stylem.css">';
		echo $_GET['name'];

		// echo ' <div class="plans">
  //   <div class="plan">
  //     <h2 class="plan-title">Oportunidades Comerciales</h2>
  //     <p class="plan-price">8<span>%</span></p>
  //     <ul class="plan-features">
  //       <li><strong>10</strong> Formularios</li>
  //     </ul>
  //   </div>
  //   <div class="plan plan-tall">
  //     <h2 class="plan-title">Respuesta a Requerimiento</h2>
  //     <p class="plan-price">4<span>%</span></p>
  //     <ul class="plan-features">
  //       <li><strong>10</strong> Formularios</li>
  //     </ul>
  //   </div>
  //   <div class="plan">
  //     <h2 class="plan-title">Potenciales Inversionistas</h2>
  //     <p class="plan-price">8<span>%</span></p>
  //     <ul class="plan-features">
  //       <li><strong>10</strong> Formularios</li>
  //     </ul>
  //   </div>
  // </div>';

		echo'<script src="http://code.highcharts.com/stock/highstock.js"></script>
<script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
';

echo '<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>';
echo "<script>
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Tabla de medicion variable'
        },
        subtitle: {
            text: 'Source: rree.gob.sv'
        },
        xAxis: {
            categories: ['0', ],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: '',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        plotOptions: {
            bar: 
                       {
               lineWidth:2,
                               dataLabels: 
                               {
                                       enabled: true,
                                       formatter:function() 
                                       {
                                               var pcnt = (this.y);
                                               var serie=this.series;
                                               return Highcharts.numberFormat(pcnt,0) + ' - ' +serie.options.dato +'%';
                                       }
                               },
                               enableMouseTracking: true
                       }
                       
        },
        
        credits: {
            enabled: false
        },
       series: [{
                type: 'bar',
                name: 'Potenciales Inversionistas',
                dato: $Porciento_contactos,
                composition: {
                    'Total %': '8%',
                    'Total ': '10'
                },
                data: [$potenci_inversio_total ]
            }, {
                type: 'bar',
                name: 'Respuesta a Requerimientos',
                dato: $Porciento_cuenta,
                composition: {
                    'Total %': '4%',
                    'Total ': '10'
                },
                data: [$resp_a_requeri_total ]
            }, {
                type: 'bar',
                name: 'Oportunidades Comerciales ',
                color: 'red',
                dato: $Porciento_oportunidades,
                composition: {
                    'Total %': '8%',
                    'Total ': '15'
                    
                },
                data: [$opor_comercial_total]
                    
            }],

        tooltip: {
            shared: false,
            formatter: function() {
                var serie = this.series;
                var s = '<b>' + Highcharts.dateFormat('%A, %b %e, %Y', this.x) + '</b><br>';
                s += '<span style=color:' + serie.color + '>' + serie.options.name + '</span>: <b>' + this.y + '</b><br/>';
                $.each(serie.options.composition, function(name, value) {
                    s += '<b>' + name + ':</b> ' + value + '<br>';
                });
                return s;
            }
        }
    });
});
</script>";

// echo '<script src="modules/q1w2e_probando/grafico.js"></script>';



		echo '    <div id="gauge" class="300x260px"  ></div>';
		echo '    <script>
    var g = new JustGage({
    id: "gauge",
    value: "'.$total.'",
    min: 0,
    max: 20,
    title: "Total",
    label: "%"
    });
   
    </script>';
	}

}

 ?>	