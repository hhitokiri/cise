<?php
$module_name = 'a123_ContactoEnLaEmpresa';
$viewdefs [$module_name] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'nombre_contac',
            'label' => 'LBL_NOMBRE_CONTAC',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'cargo_contac',
            'label' => 'LBL_CARGO_CONTAC',
          ),
          1 => 
          array (
            'name' => 'tel_contac',
            'label' => 'LBL_TEL_CONTAC',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email_contac',
            'label' => 'LBL_EMAIL_CONTAC',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'idioma_b_c',
            'studio' => 'visible',
            'label' => 'LBL_IDIOMA_B',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'contacto_generado_c',
            'studio' => 'visible',
            'label' => 'LBL_CONTACTO_GENERADO',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'conta_gene_otro_c',
            'label' => 'LBL_CONTA_GENE_OTRO',
          ),
        ),
        6 => 
        array (
          0 => '',
        ),
      ),
    ),
  ),
);
?>
