<?php
$module_name = 'a123_Referenciascomerciales';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'nombre_contactv',
            'label' => 'LBL_NOMBRE_CONTACTV',
          ),
          1 => 
          array (
            'name' => 'cargo_v',
            'label' => 'LBL_CARGO_V',
          ),
        ),
        1 => 
        array (
          1 => 
          array (
            'name' => 'tel_v',
            'label' => 'LBL_TEL_V',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email_v',
            'label' => 'LBL_EMAIL_V',
          ),
          1 => 
          array (
            'name' => 'alc_geo',
            'label' => 'LBL_ALC_GEO',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'ven_anu',
            'label' => 'LBL_VEN_ANU',
          ),
          1 => 
          array (
            'name' => 'tip_pro',
            'studio' => 'visible',
            'label' => 'LBL_TIP_PRO',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'nu_bode',
            'label' => 'LBL_NU_BODE',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'coment_v',
            'studio' => 'visible',
            'label' => 'LBL_COMENT_V',
          ),
        ),
      ),
    ),
  ),
);
?>
