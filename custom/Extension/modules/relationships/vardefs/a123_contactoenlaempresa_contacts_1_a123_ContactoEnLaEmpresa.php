<?php
// created: 2015-02-16 08:58:28
$dictionary["a123_ContactoEnLaEmpresa"]["fields"]["a123_contactoenlaempresa_contacts_1"] = array (
  'name' => 'a123_contactoenlaempresa_contacts_1',
  'type' => 'link',
  'relationship' => 'a123_contactoenlaempresa_contacts_1',
  'source' => 'non-db',
  'module' => 'Contacts',
  'bean_name' => 'Contact',
  'vname' => 'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_CONTACTS_TITLE',
);
