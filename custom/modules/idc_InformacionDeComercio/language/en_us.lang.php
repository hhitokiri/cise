<?php
// created: 2015-04-23 11:02:20
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Trading Information',
  'LNK_LIST' => 'View Trading Information',
  'LNK_IMPORT_IDC_INFORMACIONDECOMERCIO' => 'Importar Trading Information',
  'LBL_LIST_FORM_TITLE' => 'Trading Information List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Trading Information',
);