{
	"properties": [

	{

		"title":"Respuesta a la Campaña por Actividad del Destinatario"
,
		"subtitle":""
,
		"type":"horizontal group by chart"
,
		"legend":"on"
,
		"labels":"value"
,
		"thousands":""

	}

	],

	"label": [

		"Contacts"
,
		"Users"
,
		"Prospects"
,
		"Leads"
,
		"Accounts"

	],

	"color": [

		"#8c2b2b"
,
		"#468c2b"
,
		"#2b5d8c"
,
		"#cd5200"
,
		"#e6bf00"
,
		"#7f3acd"
,
		"#00a9b8"
,
		"#572323"
,
		"#004d00"
,
		"#000087"
,
		"#e48d30"
,
		"#9fba09"
,
		"#560066"
,
		"#009f92"
,
		"#b36262"
,
		"#38795c"
,
		"#3D3D99"
,
		"#99623d"
,
		"#998a3d"
,
		"#994e78"
,
		"#3d6899"
,
		"#CC0000"
,
		"#00CC00"
,
		"#0000CC"
,
		"#cc5200"
,
		"#ccaa00"
,
		"#6600cc"
,
		"#005fcc"

	],

	"values": [

	{

		"label": "Ninguno",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23None"
,
			"%23None"
,
			"%23None"
,
			"%23None"
,
			"%23None"


		]

	}
,
	{

		"label": "Mensaje Enviado/Intentado",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23targeted"
,
			"%23targeted"
,
			"%23targeted"
,
			"%23targeted"
,
			"%23targeted"


		]

	}
,
	{

		"label": "Mensajes Rebotados,Otra causa",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23send+error"
,
			"%23send+error"
,
			"%23send+error"
,
			"%23send+error"
,
			"%23send+error"


		]

	}
,
	{

		"label": "Mensajes Rebotados,Email no válido",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23invalid+email"
,
			"%23invalid+email"
,
			"%23invalid+email"
,
			"%23invalid+email"
,
			"%23invalid+email"


		]

	}
,
	{

		"label": "Enlace",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23link"
,
			"%23link"
,
			"%23link"
,
			"%23link"
,
			"%23link"


		]

	}
,
	{

		"label": "Mensaje Visto",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23viewed"
,
			"%23viewed"
,
			"%23viewed"
,
			"%23viewed"
,
			"%23viewed"


		]

	}
,
	{

		"label": "Descartado",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23removed"
,
			"%23removed"
,
			"%23removed"
,
			"%23removed"
,
			"%23removed"


		]

	}
,
	{

		"label": "Clientes Potenciales Creados",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23lead"
,
			"%23lead"
,
			"%23lead"
,
			"%23lead"
,
			"%23lead"


		]

	}
,
	{

		"label": "Contactos Creados",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23contact"
,
			"%23contact"
,
			"%23contact"
,
			"%23contact"
,
			"%23contact"


		]

	}
,
	{

		"label": "Excluidos por dirección o dominio",

		"gvalue": "0",

		"gvaluelabel": "0",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"0"
,
			"0"
,
			"0"
,
			"0"
,
			"0"


		],

		"links": [
			"%23blocked"
,
			"%23blocked"
,
			"%23blocked"
,
			"%23blocked"
,
			"%23blocked"


		]

	}

	]

}