<?php
// created: 2015-04-23 11:03:42
$mod_strings = array (
  'LBL_NOMBRE_EMP' => 'Empresa',
  'LBL_TEL_C' => 'Telefono',
  'LBL_NOMBRE_CONTAC' => 'Nombre de Contacto',
  'LBL_EMAIL_C' => 'Email',
  'LBL_CARGO_C' => 'Cargo',
  'LBL_PAIS' => 'Pais',
  'LBL_DESCRIPTION' => 'Descripción',
  'LBL_SUCURSAL' => 'Numero de sucursales',
  'LBL_HOMEPAGE_TITLE' => 'Mi Líneas de Productos (C.Comerciales)',
);