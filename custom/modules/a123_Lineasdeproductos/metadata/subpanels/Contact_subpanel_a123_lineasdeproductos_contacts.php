<?php
// created: 2015-04-24 08:58:41
$subpanel_layout['list_fields'] = array (
  'sucursal_c' => 
  array (
    'type' => 'varchar',
    'default' => true,
    'vname' => 'LBL_SUCURSAL',
    'width' => '10%',
  ),
  'description' => 
  array (
    'type' => 'text',
    'vname' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '70%',
    'default' => true,
  ),
  'edit_button' => 
  array (
    'vname' => 'LBL_EDIT_BUTTON',
    'widget_class' => 'SubPanelEditButton',
    'module' => 'a123_Lineasdeproductos',
    'width' => '4%',
    'default' => true,
  ),
  'date_modified' => 
  array (
    'vname' => 'LBL_DATE_MODIFIED',
    'width' => '45%',
    'default' => true,
  ),
  'remove_button' => 
  array (
    'vname' => 'LBL_REMOVE',
    'widget_class' => 'SubPanelRemoveButton',
    'module' => 'a123_Lineasdeproductos',
    'width' => '5%',
    'default' => true,
  ),
);