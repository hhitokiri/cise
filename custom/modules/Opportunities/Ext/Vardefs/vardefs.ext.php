<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2015-03-04 13:48:52
$dictionary['Opportunity']['fields']['sec_confec_c']['labelValue']='Si el producto es del sector confeccion (Paquete completo o CMT):';

 

 // created: 2015-03-11 14:13:28
$dictionary['Opportunity']['fields']['fec_req_c']['labelValue']='Fecha Requerida del Producto';

 

 // created: 2015-03-11 14:11:58
$dictionary['Opportunity']['fields']['uni_med_c']['labelValue']='Unidad de Medida';

 

 // created: 2015-03-11 14:12:40
$dictionary['Opportunity']['fields']['frac_aran_c']['labelValue']='Fracción Arancelaria';

 

 // created: 2015-03-11 14:11:18
$dictionary['Opportunity']['fields']['espe_tec_c']['labelValue']='Especificaciones Tecnicas';

 

 // created: 2015-03-11 14:12:11
$dictionary['Opportunity']['fields']['volu_des_c']['labelValue']='Volumen Deseado';

 

 // created: 2015-03-11 12:28:48
$dictionary['Opportunity']['fields']['name']['comments']='Name of the opportunity';
$dictionary['Opportunity']['fields']['name']['merge_filter']='disabled';
$dictionary['Opportunity']['fields']['name']['full_text_search']=array (
);

 

 // created: 2015-03-24 09:22:57
$dictionary['Opportunity']['fields']['tipo_oportunidad_c']['labelValue']='Tipo';

 

 // created: 2015-03-04 13:19:03
$dictionary['Opportunity']['fields']['website_c']['labelValue']='WebSite';

 

 // created: 2015-03-11 14:14:02
$dictionary['Opportunity']['fields']['for_pag_c']['labelValue']='Forma de pago ';

 

 // created: 2015-03-11 14:10:47
$dictionary['Opportunity']['fields']['caract_c']['labelValue']='Caracteristicas';

 

 // created: 2015-03-04 13:16:12
$dictionary['Opportunity']['fields']['direccion_c']['labelValue']='Direccion';

 

 // created: 2015-03-24 09:14:09
$dictionary['Opportunity']['fields']['nombre_c']['labelValue']='Nombre de empresa (Company name)';

 

 // created: 2015-03-04 13:16:29
$dictionary['Opportunity']['fields']['telefono_c']['labelValue']='Telefono';

 

 // created: 2015-03-24 09:13:27
$dictionary['Opportunity']['fields']['nombre1_c']['labelValue']='Nombre de Empresa (Company Name)';

 

 // created: 2015-03-25 10:53:26
$dictionary['Opportunity']['fields']['sector_opor_servicios_c']['labelValue']='Sector';

 

 // created: 2015-03-04 13:18:37
$dictionary['Opportunity']['fields']['fax_c']['labelValue']='Fax';

 

 // created: 2015-03-04 14:27:45
$dictionary['Opportunity']['fields']['actividad_c']['labelValue']='Actividad Principal (Sector)';

 

 // created: 2015-03-24 09:20:12
$dictionary['Opportunity']['fields']['caracteristicas_servicio_c']['labelValue']='Características del  servicio: (Specifications and requirements for the service)';

 

 // created: 2015-03-11 14:13:14
$dictionary['Opportunity']['fields']['req_espc_c']['labelValue']='Requerimientos Especiales';

 
?>