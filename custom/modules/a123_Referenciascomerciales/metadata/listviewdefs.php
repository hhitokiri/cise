<?php
$module_name = 'a123_Referenciascomerciales';
$listViewDefs [$module_name] = 
array (
  'TEL_V' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_TEL_V',
    'width' => '10%',
    'default' => true,
  ),
  'CARGO_V' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_CARGO_V',
    'width' => '10%',
    'default' => true,
  ),
  'COMENT_V' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_COMENT_V',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'NOMBRE_CONTACTV' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_NOMBRE_CONTACTV',
    'width' => '10%',
    'default' => false,
  ),
);
?>
