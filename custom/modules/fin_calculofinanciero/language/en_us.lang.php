<?php
// created: 2015-04-23 11:01:04
$mod_strings = array (
  'LNK_NEW_RECORD' => 'Create Financial Calculus',
  'LNK_LIST' => 'View Financial Calculus',
  'LNK_IMPORT_FIN_CALCULOFINANCIERO' => 'Importar Financial Calculus',
  'LBL_LIST_FORM_TITLE' => 'Financial Calculus List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Financial Calculus',
);