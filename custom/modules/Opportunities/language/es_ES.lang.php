<?php
// created: 2015-04-23 11:30:28
$mod_strings = array (
  'LBL_NOMBRE' => 'Nombre de empresa (Company name)',
  'LBL_DIRECCION' => 'Direccion',
  'LBL_TELEFONO' => 'Telefono',
  'LBL_FAX' => 'Fax',
  'LBL_WEBSITE' => 'WebSite',
  'LBL_UNI_MED' => 'Unidad de Medida',
  'LBL_VOLU_DES' => 'Volumen Deseado',
  'LBL_FOR_PAG' => 'Forma de pago ',
  'LBL_SEC_CONFEC' => 'Si el producto es del sector confección (Paquete completo o CMT) ',
  'LBL_FEC_REQ' => 'Fecha Requerida del Producto',
  'LBL_EDITVIEW_PANEL1' => 'Datos de la Empresa',
  'LBL_EDITVIEW_PANEL2' => 'Datos del Producto',
  'LBL_ACTIVIDAD' => 'Actividad Principal (Sector)',
  'LBL_CARACT' => 'Características ',
  'LBL_ESPE_TEC' => 'Especificaciones Técnicas ',
  'LBL_FRAC_ARAN' => 'Fracción Arancelaria',
  'LBL_REQ_ESPC' => 'Requerimientos Especiales',
  'LBL_OPPORTUNITY_NAME' => 'Producto de Interés ',
  'LBL_SECTOR_OPOR_SERVICIOS' => 'Sector',
  'LBL_NOMBRE1' => 'Nombre de Empresa (Company Name)',
  'LBL_EDITVIEW_PANEL3' => 'Datos generales del servicio',
  'LBL_CARACTERISTICAS_SERVICIO' => 'Características del  servicio: (Specifications and requirements for the service)',
  'LBL_EDITVIEW_PANEL4' => 'Descripción específica servicio solicitado (Specific description of service)',
  'LBL_EDITVIEW_PANEL5' => 'Referencias Comerciales (Trade References)',
  'LBL_TIPO_OPORTUNIDAD' => 'Tipo',
  'LBL_EDITVIEW_PANEL6' => 'Seleccione tipo de oportunidad',
);