<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


require_once('include/MVC/View/views/view.detail.php');

class AccountsViewDetail extends ViewDetail {


 	function AccountsViewDetail(){
 		parent::ViewDetail();
 	}

 	/**
 	 * display
 	 * Override the display method to support customization for the buttons that display
 	 * a popup and allow you to copy the account's address into the selected contacts.
 	 * The custom_code_billing and custom_code_shipping Smarty variables are found in
 	 * include/SugarFields/Fields/Address/DetailView.tpl (default).  If it's a English U.S.
 	 * locale then it'll use file include/SugarFields/Fields/Address/en_us.DetailView.tpl.
 	 */
 	function display(){
				
		if(empty($this->bean->id)){
			global $app_strings;
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}				
		
		$this->dv->process();
		global $mod_strings;
		if(ACLController::checkAccess('Contacts', 'edit', true)) {
			$push_billing = '<span class="id-ff"><button class="button btn_copy" title="' . $mod_strings['LBL_PUSH_CONTACTS_BUTTON_LABEL'] . 
								 '" type="button" onclick=\'open_contact_popup("Contacts", 600, 600, "&account_name=' .
								 urlencode($this->bean->name) . '&html=change_address' .
								 '&primary_address_street=' . str_replace(array("\rn", "\r", "\n"), array('','','<br>'), urlencode($this->bean->billing_address_street)) . 
								 '&primary_address_city=' . $this->bean->billing_address_city . 
								 '&primary_address_state=' . $this->bean->billing_address_state . 
								 '&primary_address_postalcode=' . $this->bean->billing_address_postalcode . 
								 '&primary_address_country=' . $this->bean->billing_address_country .
								 '", true, false);\' value="' . $mod_strings['LBL_PUSH_CONTACTS_BUTTON_TITLE']. '">'.
								 SugarThemeRegistry::current()->getImage("id-ff-copy","",null,null,'.png',$mod_strings["LBL_COPY"]).
								 '</button></span>';
								 
			$push_shipping = '<span class="id-ff"><button class="button btn_copy" title="' . $mod_strings['LBL_PUSH_CONTACTS_BUTTON_LABEL'] . 
								 '" type="button" onclick=\'open_contact_popup("Contacts", 600, 600, "&account_name=' .
								 urlencode($this->bean->name) . '&html=change_address' .
								 '&primary_address_street=' . str_replace(array("\rn", "\r", "\n"), array('','','<br>'), urlencode($this->bean->shipping_address_street)) .
								 '&primary_address_city=' . $this->bean->shipping_address_city .
								 '&primary_address_state=' . $this->bean->shipping_address_state .
								 '&primary_address_postalcode=' . $this->bean->shipping_address_postalcode .
								 '&primary_address_country=' . $this->bean->shipping_address_country .
								 '", true, false);\' value="' . $mod_strings['LBL_PUSH_CONTACTS_BUTTON_TITLE'] . '">'.
								  SugarThemeRegistry::current()->getImage("id-ff-copy",'',null,null,'.png',$mod_strings['LBL_COPY']).
								 '</button></span>';
		} else {
			$push_billing = '';
			$push_shipping = '';
		}

		$this->ss->assign("custom_code_billing", $push_billing);
		$this->ss->assign("custom_code_shipping", $push_shipping);
        
        if(empty($this->bean->id)){
			global $app_strings;
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}				
		echo $this->dv->display();

			echo <<<EOHTML
			<script>
    $(document).ready(function() {                        
    console.log('hola mundo 33');
               var Nombre= $('#name').val();
           $("#name").prepend("<option value='"+Nombre+"' selected='selected'></option>"); 
           function buscartd(panel){
    var texto = $(panel).find('td');
    console.log(texto);

}
function buscarTH(panel){
    var texto = $(panel).find('th');
    console.log(texto);

}

var pa='#detailpanel_7';
var a1 = '#whole_subpanel_accounts_idc_informaciondecomercio_1';
//buscartd(a1);
//buscarTH(a1);
//buscartd(pa);
var a2 = ['8','10'];
var th = ['3','5'];
function escondertd(panel,id){
    var texto = $(panel).find('td');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).hide(); 
});
}

 
function esconderTH(panel,id){
    var texto = $(panel).find('th');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).hide(); 
});
}

function mostrartd(panel,id){
    var texto = $(panel).find('td');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).show(); 
});
}

function mostrarTH(panel,id){
    var texto = $(panel).find('th');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).show(); 
});
}

function esconderCampos(panel,id){
    var texto = $(panel).find('td');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).hide(); 
});
}

function mostrarCampos(panel,id){
    var texto = $(panel).find('td');
    //console.log(texto);
var a = id;

a.forEach(function(entry){
    $(texto[entry]).show(); 
});
}

var tex=$('#detailpanel_2').find('td');
//console.log(tex);
function hide(id){

    var aa = id;
    aa.forEach(function(entry){
    $(tex[entry]).hide(); 
});
}

var te =$('#detailpanel_5').find('td');
//console.log(tex);
function hidepanel5(id){

    var aa2 = id;
    aa2.forEach(function(entry){
    $(te[entry]).hide(); 
});
}

var tipo = $('#tipo_servicio_c').val();
var panel1 = '#detailpanel_2';
var panel2 = '#detailpanel_5';
var panel3 = '#detailpanel_7';
//se definen numero de td a ocultar.
var camposOcultar = ['28','29','30','31','32','33','34','35','36','37','38','39','40','41','42'];
var hide2 = ['23','24','25','26','27','28','29','30'];
var hide3= ['24','25'];
var camposOcultar2 = ['12','13'];
var camposOcultar3 = ['24','25','34','35'];//panel5
var ocultar = ['0','1','2','3','4','5','6','7','8','9','10','11'];
var ocultar2 = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23'];//panel5
var ocultar3 = ['36','37'];//panel5

        if(tipo == '1'){
            mostrartd(a1,a2);
            mostrarTH(a1,th);            
            $('#detailpanel_6').show();
            mostrarCampos(panel3, ocultar);
            esconderCampos(panel3, camposOcultar2);
            mostrarCampos(panel2, camposOcultar3);
            esconderCampos(panel2, ocultar3);
            //esconderCampos(panel1, camposOcultar);
            hide(hide2);
            esconderCampos(panel2, camposOcultar2);
            esconderCampos(panel2, ocultar2);
            $("#servicios_bienes_c option[value='3']").show();
        }else{//servicios
            //hidepanel5(hide3);
            escondertd(a1,a2);
            esconderTH(a1,th);
            $('#detailpanel_6').hide();
            mostrarCampos(panel2, ocultar3);
            mostrarCampos(panel2, ocultar2);
            esconderCampos(panel3, ocultar);
            esconderCampos(panel2, camposOcultar3);
            mostrarCampos(panel1, camposOcultar);
            mostrarCampos(panel2, camposOcultar2);
            mostrarCampos(panel3, camposOcultar2);
            $("#servicios_bienes_c option[value='3']").hide();

}

});
</script>
EOHTML;
 	}

}


?>