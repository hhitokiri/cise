<?php
 // created: 2015-02-16 08:58:28
$layout_defs["Contacts"]["subpanel_setup"]['a123_contactoenlaempresa_contacts_1'] = array (
  'order' => 100,
  'module' => 'a123_ContactoEnLaEmpresa',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_A123_CONTACTOENLAEMPRESA_CONTACTS_1_FROM_A123_CONTACTOENLAEMPRESA_TITLE',
  'get_subpanel_data' => 'a123_contactoenlaempresa_contacts_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
