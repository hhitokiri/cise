<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


require_once('include/MVC/View/views/view.detail.php');

class ContactsViewDetail extends ViewDetail
{
 	/**
 	 * @see SugarView::display()
	 *
 	 * We are overridding the display method to manipulate the portal information.
 	 * If portal is not enabled then don't show the portal fields.
 	 */
 	public function display() 	
 	{
 		global $current_user;
 		$isEnabledRole = in_array("test", ACLRole::getUserRoleNames($current_user->id));

        $admin = new Administration();
        $admin->retrieveSettings();
        if(isset($admin->settings['portal_on']) && $admin->settings['portal_on']) {
           $this->ss->assign("PORTAL_ENABLED", true);
        }
 		parent::display();
 		require_once("modules/ACLRoles/ACLRole.php");
        
        global $current_user; 
        
        $role_to_check = "test";
        $acl_role_obj = new ACLRole();
        $user_roles = $acl_role_obj->getUserRoles($current_user->id);
        //if ( in_array($role_to_check,$user_roles)){
        
 		if(file_exists("cache/modules/Contacts/DetailView.tpl"))
      		unlink("cache/modules/Contacts/DetailView.tpl"); 
        //if ( in_array($role_to_check,$user_roles)){

 		echo <<<EOHTML

<script>
$(document).ready(function () {	
    //$(' #whole_subpanel_activities').hide();
    /*$('#tipo_contac_c').on('change', function (e) {
        var val = e.currentTarget.val();
        console.log(val);
		location.reload();

    });*/

    
//// esconde los campos 'contacto generado en' y 'otra forma de generar contacto'... los esconde de las siguientes fichas de contacto
////-,comercio,turismo,gremiales,medios especializados        
function Hide(){        
$("#detailpanel_4 tr > td:contains('Contacto generado en:')").html("");
$("#detailpanel_4 tr > td:contains('Especifique otra forma de generación de contacto:')").html("");                
}

function esconderRazonSocial(){
    var texto = $('#LBL_EDITVIEW_PANEL12').find('td');
    var objeto = $(texto[2]);
    var objeto1 = $(texto[3]);
    objeto.text('');
    objeto1.text('');
}
///

	var tipo = $('#tipo_contac_c').val();
	var comercio_paneles_ocultos =['5','6','7','8','9','3'];
	var inversion_paneles_ocultos =['5'];
	var turismo_paneles_ocultos =['6','7','8','9','3'];
     //   var camaras_gremiales =['10'];
     //   var medios_especializados =['11'];
     //   var oportunidades_comerciales =['12'];
        var ocultar_grem_medios_oportu =['10','11','12']; 
        var label = new Array('Datos de la empresa',' (Potencial comprador)',' Informacion sobre la empresa','Datos generales empresa','Informacion de la cámara o gremial empresarial','Datos generales del medio de comunicacion');
         
	if (tipo == '1'){
            Hide();
            
               $('#cont_generado_c_label').hide();
               $('#cont_generado_c').hide();
               $('#whole_subpanel_project').hide();                 
               $('#whole_subpanel_contacts').hide();
		console.log(tipo);
                $('#detailpanel_14').hide();

                $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = 'Datos de la empresa'.concat(label[1]);
		//panel de la parte baja de lasrelationships
		$('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();
                $('#whole_subpanel_contacts').hide();                
		/*paneles que estan dentro de el mismo formulario
		 de contactos*/
		comercio_paneles_ocultos.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
                });
                ocultar_grem_medios_oportu.forEach(function(entry){
                $('#detailpanel_'+entry).hide(); 
            });
                
	}else if (tipo == '0') {
               Hide();
               esconderRazonSocial();
		console.log(tipo); 
               $('#whole_subpanel_project').hide(); 
               $('#whole_subpanel_contacts').hide();   
               $('#detailpanel_14').hide(); 
               ocultar_grem_medios_oportu.forEach(function(entry){
               $('#detailpanel_'+entry).hide(); 
            });
		//panel de la parte baja de lasrelationships
                $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = label[0];
		$('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();
		$('#whole_subpanel_a123_lineasdeproductos_contacts_1').hide();
		$('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
		$('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
                $('#whole_subpanel_contacts_a123_referenciascomerciales_1').hide();               
                $('#whole_subpanel_contacts_rcc_referenciasc_empresas_compra_1').hide();
		/*paneles que estan dentro de el mismo formulario
		 de contactos*/
		comercio_paneles_ocultos.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});
	}else if (tipo == '2') {
                Hide();
                esconderRazonSocial();
                $('#whole_subpanel_project').hide(); 
                $('#whole_subpanel_contacts').hide();  
                $('#detailpanel_14').hide();
		console.log(tipo);
                $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = label[2];
                ocultar_grem_medios_oportu.forEach(function(entry){
                $('#detailpanel_'+entry).hide(); 
                });                
		//panel de la parte baja de lasrelationships
		$('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();
		$('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
		$('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
                $('#whole_subpanel_contacts_a123_referenciascomerciales_1').hide();               
                $('#whole_subpanel_contacts_rcc_referenciasc_empresas_compra_1').hide();                
		//$('#whole_subpanel_a123_referenciascomerciales_contacts_2').hide();
		/*paneles que estan dentro de el mismo formulario
		 de contactos*/
		turismo_paneles_ocultos.forEach(function(entry) {
		    $('#detailpanel_'+entry).hide();
		});
	}else if (tipo == '3') {
                $('#whole_subpanel_project').hide(); 
                $('#whole_subpanel_contacts').hide();  
                $('#detailpanel_14').hide();
                $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = label[3];
		console.log(tipo);
                ocultar_grem_medios_oportu.forEach(function(entry){
                $('#detailpanel_'+entry).hide(); 
                });                
		//panel de la parte baja de lasrelationships
		$('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
		$('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
                $('#whole_subpanel_contacts_a123_referenciascomerciales_1').hide();               
                $('#whole_subpanel_contacts_rcc_referenciasc_empresas_compra_1').hide();                
		//$('#whole_subpanel_a123_referenciascomerciales_contacts_2').hide();
		/*paneles que estan dentro de el mismo formulario
		 de contactos*/
		$('#detailpanel_5').hide();
	}else if (tipo == '4') {// se mostrara el detailpanel 10
            Hide();
            esconderRazonSocial();          
            $('#whole_subpanel_project').hide(); 
            $('#whole_subpanel_contacts').hide();  
            $('#detailpanel_14').hide();
            $('#whole_subpanel_contacts_a123_referenciascomerciales_1').hide();               
            $('#whole_subpanel_contacts_rcc_referenciasc_empresas_compra_1').hide();            
            console.log(tipo);
            $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = label[4];
            ocultar_grem_medios_oportu.forEach(function(entry){
               $('#detailpanel_'+entry).hide(); 
            });
            $('#detailpanel_10').show();
            $('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
            $('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
            $('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();            
            comercio_paneles_ocultos.forEach(function(entry) {
            $('#detailpanel_'+entry).hide();
            });          
        }else if (tipo == '5') {// se mostrara el detailpanel 11
            Hide();
            esconderRazonSocial();
            $('#whole_subpanel_project').hide(); 
            $('#whole_subpanel_contacts').hide();  
            $('#detailpanel_14').hide();
            $('#whole_subpanel_contacts_a123_referenciascomerciales_1').hide();               
            $('#whole_subpanel_contacts_rcc_referenciasc_empresas_compra_1').hide();
            console.log(tipo);
            $('#detailpanel_2 > h4').contents(':not(*)')[2].nodeValue = label[5];
            ocultar_grem_medios_oportu.forEach(function(entry){
               $('#detailpanel_'+entry).hide(); 
            });
            $('#detailpanel_11').show();
            $('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
            $('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
            $('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();            
            comercio_paneles_ocultos.forEach(function(entry) {
            $('#detailpanel_'+entry).hide();
            });
            inversion_paneles_ocultos.forEach(function(entry) {
                $('#detailpanel_'+entry).hide();
            });             
        } 
//        
//        else if (tipo == '6'){// se mostrara el detailpanel 12
//            ocultar_grem_medios_oportu.forEach(function(entry){
//               $('#detailpanel_'+entry).hide(); 
//            });
//            $('#detailpanel_12').show();
//            $('#whole_subpanel_a123_lineasdeproductos_contacts').hide();
//            $('#whole_subpanel_a123_referenciascomerciales_contacts_1').hide();
//            $('#whole_subpanel_a123_contactoenlaempresa_contacts_1').hide();            
//            comercio_paneles_ocultos.forEach(function(entry) {
//            $('#detailpanel_'+entry).hide();
//            });
//            inversion_paneles_ocultos.forEach(function(entry) {
//                $('#detailpanel_'+entry).hide();
//            });
//        }
//        var removerdatosrqueridosEmpresa =  [{'objeto':'account_name','label':'LBL_ACCOUNT_NAME'}
//                    ];
//            removerdatosrqueridosEmpresa.forEach(function(entry){            
//            removeFromValidate('EditView',entry.objeto);                        // else remove the validtion applied
//            $(""+entry.label+"").html('{$mod_strings['entry.label']}: ');
//            });     
        

  });
  
                
</script>
EOHTML;
//}
 	}
}
