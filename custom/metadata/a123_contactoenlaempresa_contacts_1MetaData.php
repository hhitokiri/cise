<?php
// created: 2015-02-16 08:58:28
$dictionary["a123_contactoenlaempresa_contacts_1"] = array (
  'true_relationship_type' => 'many-to-many',
  'from_studio' => true,
  'relationships' => 
  array (
    'a123_contactoenlaempresa_contacts_1' => 
    array (
      'lhs_module' => 'a123_ContactoEnLaEmpresa',
      'lhs_table' => 'a123_contactoenlaempresa',
      'lhs_key' => 'id',
      'rhs_module' => 'Contacts',
      'rhs_table' => 'contacts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'a123_contactoenlaempresa_contacts_1_c',
      'join_key_lhs' => 'a123_contactoenlaempresa_contacts_1a123_contactoenlaempresa_ida',
      'join_key_rhs' => 'a123_contactoenlaempresa_contacts_1contacts_idb',
    ),
  ),
  'table' => 'a123_contactoenlaempresa_contacts_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'a123_contactoenlaempresa_contacts_1a123_contactoenlaempresa_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'a123_contactoenlaempresa_contacts_1contacts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'a123_contactoenlaempresa_contacts_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'a123_contactoenlaempresa_contacts_1_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'a123_contactoenlaempresa_contacts_1a123_contactoenlaempresa_ida',
        1 => 'a123_contactoenlaempresa_contacts_1contacts_idb',
      ),
    ),
  ),
);